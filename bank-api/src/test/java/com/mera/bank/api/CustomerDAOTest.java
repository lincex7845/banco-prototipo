/**
 * 
 */
package com.mera.bank.api;

import static org.junit.Assert.*;
import java.util.List;
import java.util.Random;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import com.mera.bank.api.persistence.dao.CustomerDAO;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.api.persistence.util.QueryOperator;
import com.mera.bank.common.entities.Customer;

/**
 * @author david.mera
 *
 */
public class CustomerDAOTest {
	
	private final static CustomerDAO customersRepository = new CustomerDAO();
	private final static int MAX_CUSTOMERS = 30;
	
	private static void insertData(){
		for(int i = 1; i <= MAX_CUSTOMERS; i++){
			Customer cliente = new Customer("Julio_" + i, "12969860" + i, Boolean.FALSE);
			customersRepository.addCustomer(cliente);
		}	
	}
	
	private static void cleanData(){
		List<Customer> clientes = customersRepository.getAllCustomers();
		for(int i = 0; i < clientes.size(); i++)
			customersRepository.deleteCustomer(clientes.get(i).getCustomerDocumentId());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
			cleanData();
			insertData();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDown() throws Exception {
		cleanData();
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#getAllCustomers()}.
	 */
	@Test
	public void testGetAllCustomers() {
		List<Customer> customersToCompare = customersRepository.getAllCustomers();
		// El test de agregar se ejecuta antes, 
		// por eso se valida si la cantidad de resultados es mayor
		// o igual a la establecida previamente
		assertTrue(customersToCompare.size() >= MAX_CUSTOMERS);
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#getCustomersBy(com.mera.bank.api.persistence.util.QueryFilterUtil[])}.
	 */
	@Test
	public void testGetCustomersBy() {
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		QueryFilterUtil[] filters = new QueryFilterUtil[]{
				new QueryFilterUtil("customerName", QueryOperator.EQUALS, "Julio_" + id),
				new QueryFilterUtil("customerDocumentId", QueryOperator.EQUALS, "12969860" + id)
				};
		List<Customer> customers = customersRepository.getCustomersByCriteria(filters);
		assertTrue(customers.size() > 0);
		assertTrue(customers.get(0).getCustomerName().equals("Julio_"+id));
		assertTrue(customers.get(0).getCustomerDocumentId().equals("12969860"+id));
		assertTrue(customers.get(0).getIsProcessed().equals(Boolean.FALSE));
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#getCustomerById(java.lang.String)}.
	 */
	@Test
	public void testGetCustomerById() {
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		Customer customer = customersRepository.getCustomerById("12969860"+id);
		assertTrue(customer.getCustomerName().equals("Julio_"+id));
		assertTrue(customer.getCustomerDocumentId().equals("12969860"+id));
		assertTrue(customer.getIsProcessed().equals(Boolean.FALSE));
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#addCustomer(com.mera.bank.common.entities.Customer)}.
	 */
	@Test(expected=IllegalStateException.class)
	public void testAddCustomerFailed() {
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		Customer cliente = new Customer("Julio_" + id, "12969860" + id, Boolean.FALSE);
		customersRepository.addCustomer(cliente);
	}
	
	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#addCustomer(com.mera.bank.common.entities.Customer)}.
	 */
	@Test
	public void testAddCustomer() {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		customersRepository.addCustomer(cliente);
		Customer customer = customersRepository.getCustomerById(cliente.getCustomerDocumentId());
		assertNotNull(customer);
		assertTrue(customer.getCustomerName().equals(cliente.getCustomerName()));
		assertTrue(customer.getIsProcessed().equals(Boolean.FALSE));
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#updateCustomer(java.lang.String, com.mera.bank.common.entities.Customer)}.
	 */
	@Test
	public void testUpdateCustomer() {
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		Customer customerToUpdate = customersRepository.getCustomerById("12969860"+id);
		String newName = "new_Name";
		customerToUpdate.setCustomerName(newName);
		customerToUpdate.setIsProcessed(Boolean.TRUE);
		customersRepository.updateCustomer(customerToUpdate.getCustomerDocumentId(), customerToUpdate);
		Customer customer = customersRepository.getCustomerById(customerToUpdate.getCustomerDocumentId());
		assertNotNull(customer);
		assertTrue(customer.getCustomerName().equals(customerToUpdate.getCustomerName()));
		// Esta propiedad no es persistente
		assertTrue(customer.getIsProcessed().equals(Boolean.FALSE));
	}

	/**
	 * Test method for {@link com.mera.bank.api.persistence.dao.CustomerDAO#deleteCustomer(java.lang.String)}.
	 */
	@Test
	public void testDeleteCustomer() {
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		customersRepository.deleteCustomer("12969860"+id);
		Customer customer = customersRepository.getCustomerById("12969860"+id);
		assertNull(customer);
	}

}
