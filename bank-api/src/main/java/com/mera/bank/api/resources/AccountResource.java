/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.hibernate.validator.constraints.NotEmpty;
import org.jboss.logging.Logger;
import org.postgresql.util.PSQLException;
import org.springframework.dao.DataAccessException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.utilities.StringValidator;
import com.mera.bank.common.entities.Account;

/**
 * This resource process all HTTP requests related to
 *         accounts
 * @author david.mera 
 */
@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

	/**
	 * Instance of {@link com.mera.bank.api.services.BankService}
	 */
	private final BankService bankSpaceService;
	/**
	 * Used to serialize data to json format
	 */
	private static final Gson gson;
	/**
	 * logger instance
	 */
	private static final Logger LOGGER;
	/***
	 * "accountNumber" constant
	 */
	private static final String ACCOUNTNUMBERPARAM;
	/***
	 * "customerDocumentId" constant
	 */
	private static final String CUSTOMERDOCUMENTIDPARAM;

	static {
		LOGGER = Logger.getLogger(AccountResource.class.getName());
		gson = new GsonBuilder().create();
		ACCOUNTNUMBERPARAM = "accountNumber";
		CUSTOMERDOCUMENTIDPARAM = "customerDocumentId";
	}

	/**
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to accounts
	 */
	public AccountResource(BankService bankService) {
		this.bankSpaceService = bankService;
	}

	/***
	 * GET method which returns a bank account by 
	 * a given customer id and account number
	 * 
	 * @param uriInfo	
	 * 				Object which represent the URL information associated to the
	 *            	HTTP request
	 * @return
	 * 		   OK status code if any account is identified by the given number and customer id.
	 *         NOT_FOUND if the given id does not exists into the database.
	 *         INTERNAL_SERVER_ERROR if an error occurs during the process
	 */
	@GET
	public Response getBankAccountByCustomerIdAndNumber(@Context UriInfo uriInfo) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			Map<String, String> parameters = validateParameters(uriInfo.getQueryParameters());
			if (parameters.size() <= 2 && parameters.size() > 0) {
				List<Account> accounts = getAccountsByParameters(parameters);
				if (accounts.isEmpty()) {
					status = Response.Status.NOT_FOUND.getStatusCode();
					entity = "There are not accounts which satisfies the given criteria.";
				} else {
					status = Response.Status.OK.getStatusCode();
					entity = gson.toJson(accounts);
				}
			} else {
				status = Response.Status.BAD_REQUEST.getStatusCode();
				entity = "The customer's id and account's number are required to fetch accounts";
			}
		} catch (org.springframework.dao.DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = "There are not accounts which satisfies the given criteria.";
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get account by a given Id
	 * 
	 * @param accountId
	 *            The account's Id
	 * @return OK status code if any account is identified by the given id.
	 *         NOT_FOUND if the given id does not exists into the database.
	 *         INTERNAL_SERVER_ERROR if an error occurs during the process
	 */
	@GET
	@Path("/byAccountId/{accountId}")
	public Response getBankAccountById(@PathParam("accountId") @NotEmpty String accountId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			Account account = this.bankSpaceService.getAccountById(accountId);
			status = Response.Status.OK.getStatusCode();
			entity = gson.toJson(account);
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (org.springframework.dao.DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get account by a given number
	 * 
	 * @param accountNumber
	 *            The account's number
	 * @return OK status code if any account is identified by the given number.
	 *         NOT_FOUND if the given number does not exists into the database.
	 *         INTERNAL_SERVER_ERROR if an error occurs during the process
	 */
	@GET
	@Path("/byAccountNumber/{accountNumber}")
	public Response getBankAccountByNumber(@PathParam("accountNumber") @NotEmpty String accountNumber) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			Account account = this.bankSpaceService.getAccountByNumber(accountNumber);
			status = Response.Status.OK.getStatusCode();
			entity = gson.toJson(account);
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (org.springframework.dao.DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get account by a given customer's id
	 * 
	 * @param customerDocumentId
	 *            The customer's Id
	 * @return OK status code if any account is identified by the given
	 *         customer's id. NOT_FOUND exception if the given customer's id
	 *         does not exists into the database. INTERNAL_SERVER_ERROR if an
	 *         error occurs during the process
	 */
	@GET
	@Path("/byCustomer/{customerDocumentId}")
	public Response getBankAccountsByCustomerId(@PathParam("customerDocumentId") @NotEmpty String customerDocumentId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			List<Account> accounts = this.bankSpaceService.getBankAccountsByCustomerId(customerDocumentId);
			if (accounts.isEmpty()) {
				status = Response.Status.NOT_FOUND.getStatusCode();
				entity = "There are not accounts associated to customer " + customerDocumentId;
			} else {
				Collections.sort(accounts, new Comparator<Account>() {
					@Override
					public int compare(Account a, Account b) {
						return a.getId().compareTo(b.getId());
					}
				});
				status = Response.Status.OK.getStatusCode();
				entity = gson.toJson(accounts);
			}
		} catch (org.springframework.dao.DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * POST method that allows to create a new bank account
	 * 
	 * @param account
	 *            The new bank account to create
	 * 
	 * @return CREATED status code if the account was created. CONFLICT status
	 *         code if the account already exists into the database. BAD_REQUEST
	 *         status code if the passed account is null or not valid.
	 *         INTERNAL_SERVER_ERROR if an error occurs during the process
	 */
	@POST
	@Path("/add")
	public Response addBankAccount(Account account) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankAccount(account);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The account was created";
		} catch (org.openspaces.core.EntryAlreadyInSpaceException e) {
			LOGGER.error(e);
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The account already exists into the database";
		} catch (IllegalArgumentException e) {
			LOGGER.error(e);
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		}
		catch(org.openspaces.core.InternalSpaceException e){
			PSQLException internal = (PSQLException)getPSQLException(e);
			LOGGER.error(internal);
			status = Response.Status.CONFLICT.getStatusCode();
			entity = internal.getServerErrorMessage().getDetail();
		}
		catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * DELETE method created in order to delete bank accounts
	 * 
	 * @param accountId
	 *            The account's identifier
	 * @return OK status code is returned if the account was deleted NOT_FOUND
	 *         status code is returned if the account does not exist into the
	 *         database INTERNAL_SERVER_ERROR code is returned if an error
	 *         occurs during the process
	 */
	@DELETE
	@Path("/delete")
	public Response updateCustomer(@QueryParam("accountId")@NotEmpty String accountId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.deleteBankAccount(accountId);
			status = Response.Status.OK.getStatusCode();
			entity = "The account was deleted";
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * This method checks if the given parameters are valid,
	 * it means, if an account number and a customer id are given
	 * @param queryParams A map of key-valued parameters
	 * @return Map which contains the valid parameters. It can be empty if the checked parameter are not valid
	 */
	private static Map<String, String> validateParameters(MultivaluedMap<String, String> queryParams) {
		Map<String, String> parameters = new HashMap<String, String>();
		for (Entry<String, List<String>> entry : queryParams.entrySet()) {
			String key = entry.getKey();
			if(CUSTOMERDOCUMENTIDPARAM.equals(key)||ACCOUNTNUMBERPARAM.equals(key) && StringValidator.isValidString(entry.getValue().get(0))){
				parameters.put(key, entry.getValue().get(0));
			}
		}
		return parameters;
	}

	/***
	 * This method gets a account by a given account number and customer id
	 * @param parameters A map of key-valued parameters
	 * @return
	 * @throws org.springframework.dao.DataAccessException
	 *             In the event of a read error, DataAccessException will wrap a
	 *             ReadMultipleException, accessible via
	 *             DataAccessException.getRootCause()
	 * @throws NoSuchElementException
	 *             if the required account does not exists
	 */
	private List<Account> getAccountsByParameters(Map<String, String> parameters) {
		List<Account> accounts = new ArrayList<Account>();
		String accountNumber = parameters.get(ACCOUNTNUMBERPARAM);
		String customerDocumentId = parameters.get(CUSTOMERDOCUMENTIDPARAM);
		if (parameters.size() == 1) {
			for (String key : parameters.keySet()) {
				if (ACCOUNTNUMBERPARAM.equals(key)) {
					accounts.add(bankSpaceService.getAccountByNumber(accountNumber));
				} else if (CUSTOMERDOCUMENTIDPARAM.equals(key)) {
					accounts = bankSpaceService.getBankAccountsByCustomerId(customerDocumentId);
				}
			}
		} else if (parameters.size() == 2) {
			Account account = bankSpaceService.getAccountByNumber(accountNumber);
			if (account.getCustomerDocumentId().equals(customerDocumentId))
				accounts.add(account);
		}
		return accounts;
	}
	
	/**
	 * This method gets the inner PSQLException.
	 * Use this method just for checking a {@link org.openspaces.core.InternalSpaceException} exception 
	 * @param e	The exception to check
	 * @return The inner PSQLException
	 */
	private Throwable getPSQLException(Throwable e){
		if(org.postgresql.util.PSQLException.class.isInstance(e))
			return e;
		else if(java.sql.BatchUpdateException.class.isInstance(e)){
			return getPSQLException(((java.sql.BatchUpdateException)e).getNextException());
		}
		else
			return getPSQLException(e.getCause());
	}
}
