/**
 * 
 */
package com.mera.bank.api.persistence.dao;

import java.util.List;
import javax.persistence.PersistenceException;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.api.persistence.util.QueryOperator;
import com.mera.bank.common.entities.Account;

/**
 * Data access object for manipulating bank account table
 * 
 * @author david.mera
 * @since 2015-11-20
 */
@Repository
@Qualifier(value = "accountsRepository")
public class AccountDAO extends AbstractDAO<Account> {

	/***
	 * Constructor by default
	 */
	public AccountDAO() {
		super(Account.class);
	}

	/***
	 * This method allows to insert a new bank account into the database
	 * 
	 * @param newAccount
	 *            The new bank account to insert
	 * @throws IllegalStateException
	 *             If a validation constraint violation happens
	 * @throws PersistenceException
	 *             Indicates: <br>
	 *             <list>
	 *             <ul>
	 *             An invalid configuration or invalid mapping information
	 *             </ul>
	 *             <ul>
	 *             A problem opening the session, checking the transaction
	 *             status,committing the transaction or cleaning up
	 *             </ul>
	 *             <ul>
	 *             When a null record is passed as parameter
	 *             </ul>
	 *             </list>
	 * */
	public void addAccount(Account newAccount) {
		try {
			this.saveRecord(newAccount);
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			this.LOGGER.error(e);
			throw new IllegalStateException(e.getMessage(), e);
		} catch (Exception e) {
			this.LOGGER.error(e);
			if (PersistenceException.class.isInstance(e)) {
				throw e;
			} else {
				throw new PersistenceException(e);
			}
		} finally {
			this.closeSession();
		}
	}

	/***
	 * This method allows to delete a bank account by a given id
	 * 
	 * @param accountId
	 *            the account's identifiers
	 */
	public void deleteAccount(String accountId) {
		try {
			deleteRecord(getRecordById(accountId));
		} catch (UnsupportedOperationException | HibernateException
				| PersistenceException e) {
			this.LOGGER.error(e);
			this.closeSession();
		}
	}

	/***
	 * This method allows delete a bank account by the customer's document Id
	 * which the account belongs to and its the account number
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 * @param accountNumber
	 *            The account's number which the customer identifies the account
	 */
	public void deleteAccount(String customerDocumentId, String accountNumber) {

		try {
			List<Account> accounts = this
					.getRecordsByCriteria(new QueryFilterUtil[] {
							new QueryFilterUtil("accountNumber",
									QueryOperator.EQUALS, accountNumber),
							new QueryFilterUtil("customerDocumentId",
									QueryOperator.EQUALS, customerDocumentId) });
			if (accounts.size() == 1 && accounts.get(0) != null) {
				deleteRecord(accounts.get(0));
			}
		} catch (UnsupportedOperationException | HibernateException
				| PersistenceException e) {
			this.LOGGER.error(e);
			this.closeSession();
		}
	}

	/***
	 * Return an account by several criteria
	 * 
	 * @param queryFilters
	 *            The criteria
	 * @return records within the table which satisfies the criteria.
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	public List<Account> getAccountsByCriteria(QueryFilterUtil... queryFilters) {
		return this.getRecordsByCriteria(queryFilters);
	}

	/***
	 * Return an account by a given id
	 * 
	 * @param accountId
	 *            Unique identifier
	 * @return The account identified by the given id. If a exception occurs, a
	 *         null is returned
	 */
	public Account getAccountById(String accountId) {
		Account account = null;
		try {
			account = this.getRecordById(accountId);
		} catch (Exception e) {
			this.LOGGER.error(e);
			this.closeSession();
		}
		return account;
	}
}