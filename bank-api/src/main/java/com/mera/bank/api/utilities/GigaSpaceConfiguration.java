/**
 * 
 */
package com.mera.bank.api.utilities;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A POJO class which represents the configuration required to connect the API to gigaspace
 * @author david.mera
 */
public class GigaSpaceConfiguration {

	/***
	 * The Space URL in Jini format
	 */
	@NotEmpty
	private String spaceUrl;
	/***
	 * The lookup group which the space is running
	 */
	@NotEmpty
	private String lookupGroup;
	
	/**
	 * @return the spaceUrl
	 */
	@JsonProperty
	public String getSpaceUrl() {
		return spaceUrl;
	}
	
	
	/**
	 * @return the lookupGroup
	 */
	@JsonProperty
	public String getLookupGroup() {
		return lookupGroup;
	}
	
	/**
	 * @param spaceUrl the spaceUrl to set
	 */
	public void setSpaceUrl(String spaceUrl) {
		this.spaceUrl = spaceUrl;
	}
	
	/**
	 * @param lookupGroup the lookupGroup to set
	 */
	public void setLookupGroup(String lookupGroup) {
		this.lookupGroup = lookupGroup;
	}
}