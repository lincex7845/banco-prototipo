/**
 * 
 */
package com.mera.bank.api.persistence.util;

/**
 * Utility used in order to wrap conditionals
 * @author david.mera
 *
 */
public class QueryFilterUtil {
	
	private String attribute;
	private QueryOperator operator;
	private Object value;
			
	/***
	 * The parameterized constructor
	 * @param attribute		Name of the attribute to compare
	 * @param operator		Type of operator used to perform the comparision
	 * @param value			Value to compare
	 */
	public QueryFilterUtil(String attribute, QueryOperator operator, Object value)
	{
		this.attribute = attribute;
		this.operator = operator;
		this.value = value;
	}
	
	/**
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}	

	/**
	 * @return the operator
	 */
	public QueryOperator getOperator() {
		return operator;
	}
	
	/**
	 * @return the valueToCompare
	 */
	public Object getValueToCompare() {
		return value;
	}
	
	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(QueryOperator operator) {
		this.operator = operator;
	}

	/**
	 * @param valueToCompare the valueToCompare to set
	 */
	public void setValueToCompare(Object valueToCompare) {
		this.value = valueToCompare;
	}
}