package com.mera.bank.api.persistence.util;

/***
 * Enumeration which represents some constrains and others operations 
 * performed against database server 
 * @author david.mera
 *
 */
public enum QueryOperator {
	
	/***
	 * Logic conjunction
	 */
	AND,
	/***
	 * Logic disjunction
	 */
	OR,
	/***
	 * The > arithmetic operator
	 */
	GREATER_THAN,
	/***
	 * The >= arithmetic operator
	 */
	GREATER_EQUALS_THAN,
	/***
	 * The < arithmetic operator
	 */
	LESS_THAN,
	/***
	 * The <= arithmetic operator
	 */
	LESS_EQUALS_THAN,
	/**
	 * The == arithmetic operator
	 */
	EQUALS,
	/***
	 * Similar to != or <> operators
	 */
	DIFFERENT,
	/**
	 * Represents the "like" operator in SQL
	 */
	CONTAINS,
	/**+
	 * Represents an ascending order in SQL by a given argument
	 */
	ASCENDING_ORDER_BY,
	/**+
	 * Represents an descending order in SQL by a given argument
	 */
	DESCENDING_ORDER_BY,
	/***
	 * Represents a validation which checks if the argument is equals to null
	 */
	IS_NULL,
	/***
	 * Represents a validation which checks if the argument is not equals to null
	 */
	IS_NOT_NULL,
	/***
	 * Represents a validation which checks if a value is between two given arguments 
	 */
	BETWEEN;
}