/**
 * 
 */
package com.mera.bank.api.persistence.dao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.persistence.PersistenceException;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.stat.Statistics;
import org.jboss.logging.Logger;
import com.google.gson.reflect.TypeToken;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.api.persistence.util.QueryOperator;
import com.mera.bank.api.persistence.util.SessionFactoryUtil;

/**
 * Abstract data access object which allows manipulate data on bank database
 * 
 * @author david.mera
 * @since 2015-11-20
 */
public class AbstractDAO<T> {

	/***
	 * the parameterized entity class
	 */
	private Class<T> entity;

	/***
	 * the parameterized type
	 */
	private Type entityType = new TypeToken<T>() {
	}.getType();
	
	/**
	 * Ensures that every client gets his correct session
	 */
	private static final ThreadLocal<Session> sessions = new ThreadLocal<>();

	/***
	 * This method logs
	 */
	protected final Logger LOGGER = Logger.getLogger(entityType.getClass());

	/***
	 * Statistics provided by session object
	 */
	protected Statistics stats = statistics();

	/***
	 * Default constructor
	 * 
	 * @param entity
	 *            Parameterized entity which represents a table on the database
	 */
	public AbstractDAO(Class<T> entity) {
		this.entity = entity;
	}

	/***
	 * This method initializes statistics for SessionFactoryUtil object
	 * 
	 * @return The Statistics Object
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 */
	private static Statistics statistics() {
		Statistics statics = SessionFactoryUtil.getSessionFactory()
				.getStatistics();
		statics.setStatisticsEnabled(true);
		return statics;
	}	

	/**
	 * Gets the current hibernate session. Also takes care that there's always
	 * an open hibernate transaction when needed. *
	 * 
	 * @return Current hibernate session
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session
	 */
	protected Session getSession() {
		Session result = sessions.get();
		if (result == null) {
			result = SessionFactoryUtil.getSessionFactory().openSession();
			sessions.set(result);
			result.beginTransaction();
			LOGGER.info("Transaction initialized");
		}
		return result;
	}

	/**
	 * Closes the current hibernate session, if there is one.
	 * 
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up
	 * @throws ConstraintViolationException
	 *             A constraint validation violation happened
	 */
	protected void closeSession(){
		Session sess = sessions.get();
		if (sess == null || !sess.isOpen())
			return;
		sessions.remove();
		PersistenceException error = new PersistenceException();
		try {
			if (sess.getTransaction().isActive()) {
				sess.getTransaction().commit();
				LOGGER.info("Commit");
			}
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			throw e;
		} catch (HibernateException e) {
			sess.getTransaction().rollback();
			error = new PersistenceException(e);
			LOGGER.error("Rollback", e);
		} finally {
			try {
				sess.close();
				LOGGER.info(String.format(
						"Opened Session: %d. Closed Session: %d.",
						stats.getSessionOpenCount(),
						stats.getSessionCloseCount()));
			} catch (HibernateException th) {
				LOGGER.error("Close Session failed. ", th);
				if (error != null) {
					error.addSuppressed(th);
				} else {
					error = new PersistenceException(th);
				}
			}
		}
		throw error;
	}

	/***
	 * Gets the Hibernate Criteria Instace for the entity given
	 * 
	 * @return Hibernate Criteria Instace for manipulating data
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid
	 *             mapping information
	 * @throws HibernateException
	 *             Indicates a problem opening the session
	 */
	protected Criteria getEntityCriteria() {
		return getSession().createCriteria(entity);
	}

	/***
	 * Gets the record by a given id
	 * 
	 * @param id
	 *            Unique identifier
	 * @return The record which is identified by the given id. If an exception
	 *         occurs, a null element is returned
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 */
	@SuppressWarnings("unchecked")
	protected T getRecordById(String id) {
		T entidad;
		entidad = (T) getSession().get(entity, id);
		closeSession();
		return entidad;
	}

	/***
	 * Gets all records
	 * 
	 * @return All records within the table. If an exception occurs, an empty
	 *         list is returned
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	protected List<T> getAllRecords() {
		return getRecordsByCriteria();
	}

	/***
	 * Gets records by a given criteria
	 * 
	 * @param queryFilters
	 * @return records within the table which satisfies the criteria. If an
	 *         exception occurs, an empty list is returned
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	@SuppressWarnings("unchecked")
	protected List<T> getRecordsByCriteria(QueryFilterUtil... queryFilters) {
		List<T> list = new ArrayList<T>();
		try {
			Criteria criteria = getSession().createCriteria(entity);
			for (QueryFilterUtil filter : queryFilters) {
				QueryOperator operator = filter.getOperator();
				if (operator == QueryOperator.ASCENDING_ORDER_BY
						|| operator == QueryOperator.DESCENDING_ORDER_BY)
					criteria.addOrder(convertToOrder(filter));
				else
					criteria.add(convertToCriterion(filter));
			}
			list.addAll(new HashSet<T>(criteria.list()));
			closeSession();
		} catch (HibernateException e) {
			LOGGER.error(e);
		}
		return list;
	}

	/***
	 * This method allows to update data into the database
	 * 
	 * @param newRecord
	 *            The new record to save
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             When a null record is passed as parameter
	 */
	protected void updateRecord(T newRecord) {
		if (newRecord == null)
			throw new IllegalArgumentException("Null records are not allowed");
		getSession().saveOrUpdate(newRecord);
		closeSession();
	}

	/***
	 * This method allows to insert data into the database
	 * 
	 * @param newRecord
	 *            The new record to save
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             When a null record is passed as parameter
	 */
	protected void saveRecord(T newRecord) {
		if (newRecord == null)
			throw new IllegalArgumentException("Null records are not allowed");
		getSession().save(newRecord);
		closeSession();
	}

	/***
	 * This method allows to delete a record into the database
	 * 
	 * @param record
	 *            The instance of the record to delete
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws UnsupportedOperationException
	 *             When a null record is passed as parameter
	 */
	protected void deleteRecord(T record) {
		if (record == null)
			throw new UnsupportedOperationException(
					"It is not possible to delete null elements");
		getSession().delete(record);
		closeSession();
	}

	/***
	 * This method converts a QueryFilter object to Hibernate Criterion object
	 * 
	 * @param queryFilter
	 *            The QueryFilter object to convert
	 * @return The Criterion Object which matches the QueryFilter. If the
	 *         QueryFilter does not match any Criterion Type, returns null
	 */
	protected Criterion convertToCriterion(QueryFilterUtil queryFilter) {
		QueryOperator operator = queryFilter.getOperator();
		Criterion criterion = null;
		switch (operator) {
		case EQUALS:
			criterion = Restrictions.eq(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case DIFFERENT:
			criterion = Restrictions.ne(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case GREATER_THAN:
			criterion = Restrictions.gt(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case GREATER_EQUALS_THAN:
			criterion = Restrictions.ge(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case LESS_THAN:
			criterion = Restrictions.lt(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case LESS_EQUALS_THAN:
			criterion = Restrictions.le(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case CONTAINS:
			criterion = Restrictions.ilike(queryFilter.getAttribute(),
					queryFilter.getValueToCompare());
			break;
		case IS_NULL:
			criterion = Restrictions.isNull(queryFilter.getAttribute());
			break;
		case IS_NOT_NULL:
			criterion = Restrictions.isNotNull(queryFilter.getAttribute());
			break;
		default:
			break;
		}
		return criterion;
	}

	/***
	 * This method converts a QueryFilter object to Hibernate Order object
	 * 
	 * @param queryFilter
	 *            The QueryFilter object to convert
	 * @return The Order Object which matches the QueryFilter. If the
	 *         QueryFilter does not match any Order Type, returns null
	 */
	protected Order convertToOrder(QueryFilterUtil queryFilter) {
		QueryOperator operator = queryFilter.getOperator();
		Order orden = null;
		switch (operator) {
		case ASCENDING_ORDER_BY:
			orden = Order.asc(queryFilter.getAttribute());
			break;
		case DESCENDING_ORDER_BY:
			orden = Order.desc(queryFilter.getAttribute());
			break;
		default:
			break;
		}
		return orden;
	}

}
