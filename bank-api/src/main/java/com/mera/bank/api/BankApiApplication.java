/**
 * 
 */
package com.mera.bank.api;
import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;

import com.mera.bank.api.resources.AccountResource;
import com.mera.bank.api.resources.CustomerResource;
import com.mera.bank.api.resources.MovementResource;
import com.mera.bank.api.services.BankSpaceServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author david.mera
 * Main class which is used by dropwizard to launch the application
 */
public class BankApiApplication extends Application<BankApiConfiguration> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.dropwizard.Application#initialize(io.dropwizard.setup.Bootstrap)
	 */
	@Override
	public void initialize(Bootstrap<BankApiConfiguration> arg0) {
		//Implements bootstraping by default
	}

	/***
	 * On this method the application gets some parameters in 
	 * configuration class(BankApiConfiguration), which are stored on the file
	 * bank.yml  
	 * 
	 * @param BankApiConfiguration The configuration required
	 * @param Environment	Performs the jersey container
	 * 
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration,
	 * io.dropwizard.setup.Environment)
	 */
	@Override
	public void run(BankApiConfiguration configuration, Environment environment) throws Exception {
		UrlSpaceConfigurer configurer = new UrlSpaceConfigurer(configuration.getGigaSpaceConfiguration().getSpaceUrl())
				.lookupTimeout(2000);
		GigaSpace gigaSpace = new GigaSpaceConfigurer(configurer).create();
		BankSpaceServiceImpl bankService = new BankSpaceServiceImpl(gigaSpace);
		final CustomerResource customerRestService = new CustomerResource(bankService);
		final AccountResource accountRestService = new AccountResource(bankService);
		final MovementResource movementRestService = new MovementResource(bankService);
		// register resources
		environment.jersey().register(customerRestService);
		environment.jersey().register(accountRestService);
		environment.jersey().register(movementRestService);
		// TO-DO healthcheck
		
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new BankApiApplication().run(args);
	}

	/**
	 * get the name of application
	 */
	@Override
	public String getName() {
		return "bank-api";
	}
}
