/**
 * 
 */
package com.mera.bank.api.services;

import java.util.Date;
import java.util.List;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;
import com.mera.bank.common.entities.Movement;

/**
 * A set of provided methods to manipulate data
 * 
 * @author david.mera
 *
 */
public interface BankService {

	/***
	 * This method allows to insert a new customer
	 * 
	 * @param newCustomer
	 *            The customer to insert
	 */
	public void createBankCustomer(Customer newCustomer);

	/***
	 * This method gets the customer required by a given Id and a given name
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 * @param customerName
	 *            The customer's name
	 * @return The customer which satisfies the given criteria
	 */
	public Customer getBankCustomerByIdAndName(String customerDocumentId, String customerName);

	/***
	 * Gets all bank customers
	 * 
	 * @return The list of all bank customers
	 */
	public List<Customer> getAllBankCustomers();

	/***
	 * Gets a customer by a given Id
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 * @return The customer which satisfies the given criterion
	 */
	public Customer getBankCustomerById(String customerDocumentId);

	/***
	 * This method allows to update the customer
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 * @param updatedCustomer
	 *            The customer data to set
	 */
	public void updateBankCustomer(String customerDocumentId, Customer updatedCustomer);

	/***
	 * This method allows to delete a customer by a given Id
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 */
	public void deleteBankCustomer(String customerDocumentId);

	/***
	 * This method allows to insert a new bank account
	 * 
	 * @param newAccount
	 *            The bank account to be created
	 */
	public void createBankAccount(Account newAccount);

	/***
	 * Gets the bank accounts associated to a customer
	 * 
	 * @param customerId
	 *            The customer's identifier
	 * @return List of bank accounts which belong to the customer
	 */
	public List<Account> getBankAccountsByCustomerId(String customerId);

	/***
	 * Gets an account by a given Id
	 * 
	 * @param accountId
	 *            The account's identifier
	 * @return The account which satisfies the given criterion
	 */
	public Account getAccountById(String accountId);

	/***
	 * Gets an account by a given number
	 * 
	 * @param accountNumber
	 *            The account's number
	 * @return The account which satisfies the given criterion
	 */
	public Account getAccountByNumber(String accountNumber);

	/***
	 * Deletes an account by a given Id
	 * 
	 * @param accountId
	 *            The account's identifier
	 */
	public void deleteBankAccount(String accountId);

	/***
	 * This method allows to insert a new bank movement (bank transaction)
	 * 
	 * @param newMovement
	 *            The movement to insert
	 */
	public void createBankMovement(Movement newMovement);

	/***
	 * Gets the movements which belong to a given bank account and the movements
	 * executed between a date range
	 * 
	 * @param accountNumber
	 *            The account's number
	 * @param initialDate
	 *            The initial date to include movements
	 * @param finalDate
	 *            The final date to include movements
	 * @return List of movements which satisfies the given criteria
	 */
	public List<Movement> getMovementByDateRangeAndAccountNumber(String accountNumber, Date initialDate,
			Date finalDate);

}
