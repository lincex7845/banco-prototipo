/**
 * 
 */
package com.mera.bank.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mera.bank.api.utilities.GigaSpaceConfiguration;

import io.dropwizard.Configuration;

/**
 * Represents the data stored on bank.yml file
 * @author david.mera
 *
 */
public class BankApiConfiguration extends Configuration {

	/***
	 * Instance of gigaspace configuration
	 */
	@Valid
	@NotNull
	private GigaSpaceConfiguration gigaSpaceConfiguration;

	/**
	 * @return the gigaSpaceConfiguration
	 */
	@JsonProperty
	public GigaSpaceConfiguration getGigaSpaceConfiguration() {
		return gigaSpaceConfiguration;
	}

	/**
	 * @param gigaSpaceConfiguration the gigaSpaceConfiguration to set
	 */
	public void setGigaSpaceConfiguration(GigaSpaceConfiguration gigaSpaceConfiguration) {
		this.gigaSpaceConfiguration = gigaSpaceConfiguration;
	}
}
