/**
 * 
 */
package com.mera.bank.api.persistence.util;

import javax.persistence.PersistenceException;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jboss.logging.Logger;

/**
 * This utility implements Session Factory Pattern developed by RedHat in
 * Hibernate
 * 
 * @author david.mera
 * @since 2015-11-20 *
 */
public class SessionFactoryUtil {
	
	private static final Logger LOGGER = Logger.getLogger(SessionFactoryUtil.class.getName());
	private static final SessionFactory sessionFactory;
	private static final Configuration configuration;

	static {
		try {
			// This line reads the configuration required to connect to database server
			configuration = new Configuration().configure("/hibernate.cfg.xml");
			sessionFactory = configuration.buildSessionFactory();
			LOGGER.info("Initial SessionFactory creation finished at " + new java.util.Date().toString());
		} catch (HibernateException ex) {
			LOGGER.error("Initial SessionFactory creation failed. ", ex);
			throw new PersistenceException(ex);
		}
	}
	
	/***
	 * constructor by default
	 */
	private SessionFactoryUtil(){
		
	}

	/***
	 * Gets the implemented SessionFactory object
	 * 
	 * @return The session factory utility provided by Hibernate
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}