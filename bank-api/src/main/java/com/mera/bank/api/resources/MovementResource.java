/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.utilities.StringValidator;
import com.mera.bank.common.entities.Movement;

/**
 * This resources process all HTTP requests related to
 *         movements (bank transactions)
 * @author david.mera 
 */
@Path("/movements")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MovementResource {

	/**
	 * Instance of {@link com.mera.bank.api.services.BankService}
	 */
	private final BankService bankSpaceService;
	/**
	 *  Used to serialize data to json format
	 */
	private static final Gson gson;
	/**
	 * logger instance
	 */
	private static final Logger LOGGER;
	/**
	 * the "status" constant
	 */
	private static final String STATUS;
	/**
	 * the "entity" constant
	 */
	private static final String ENTITY;
	/***
	 * This constant contains the ID for Colombia's timezone
	 */
	private static final String ZONACOLOMBIA; 

	static {
		LOGGER = Logger.getLogger(CustomerResource.class.getName());
		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
		STATUS = "status";
		ENTITY = "entity";
		ZONACOLOMBIA = "America/Bogota";
	}

	/**
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to accounts
	 */
	public MovementResource(BankService bankService) {
		bankSpaceService = bankService;
	}

	/***
	 * POST method which allows to create a new bank movement
	 * 
	 * @param movement
	 *            The new movement to create
	 * @return CREATED status code if the movement was created. CONFLICT status
	 *         code if the movement already exists into the database.
	 *         BAD_REQUEST status code if the passed movement is null or not
	 *         valid. INTERNAL_SERVER_ERROR code if an error occurs during the
	 *         process.
	 */
	@POST
	@Path("/add")
	public Response addBankMovement(Movement movement) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankMovement(movement);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The movement was created";
		} catch (org.openspaces.core.EntryAlreadyInSpaceException e) {
			LOGGER.error(e);
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The movement already exists into the database";
		} catch (IllegalArgumentException e) {
			LOGGER.error(e);
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get movements of a bank account executed
	 * during a period of time
	 * 
	 * @param uriInfo
	 *            Object which represent the URL information associated to the
	 *            HTTP request
	 * @return OK status code if some movements satisfies the given criteria.
	 *         NOT_FOUND status code if any movement satisfies the given
	 *         criteria. BAD_REQUEST status code if some of the parameters in
	 *         the URL is not valid or is null INTERNAL_SERVER_ERROR status code
	 *         if an error occurs during the process
	 */
	@GET
	public Response getMovementByDateAndAccount(@Context UriInfo uriInfo) {
		Response response;
		int status = 0;
		String entity = null;
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		try {
			if (queryParams.size() > 0) {
				Map<String, Object> parameters = checkDatesAndAccountNumber(queryParams);
				if (parameters.size() == 3) {
					Long initialDate = (Long) parameters.get("initialDate");
					Long finalDate = (Long) parameters.get("finalDate");
					String accountNumber = (String) parameters.get("accountNumber");
					List<Movement> movements = bankSpaceService.getMovementByDateRangeAndAccountNumber(accountNumber,
							new DateTime(initialDate).withZone(DateTimeZone.forID(ZONACOLOMBIA)).toDate(), new DateTime(finalDate).withZone(DateTimeZone.forID(ZONACOLOMBIA)).toDate());
					Map<String, Object> responseParameters = validateMovements(movements);
					status = (int) responseParameters.get(STATUS);
					entity = (String) responseParameters.get(ENTITY);
				} else {
					status = Response.Status.BAD_REQUEST.getStatusCode();
					entity = "The parameters sent are not allowed.";
				}
			} else {
				status = Response.Status.BAD_REQUEST.getStatusCode();
				entity = "The range of dates and account's number are required in order to fetch the movements";				
			}
		} catch (java.lang.NumberFormatException e) {
			LOGGER.error(e);
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = "Some of the date parameters sent are not valid.";
		} catch (org.springframework.dao.DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (NullPointerException e) {
			LOGGER.error(e);
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;

	}

	/***
	 * This method checks if the given query parameters are valid or not.
	 * It means if a range of dates and account number are given
	 * @param queryParams	Parameters in the URL
	 * @return	A map of key-valued parameters. It can be null if the query parameters are not valid
	 */
	private static Map<String, Object> checkDatesAndAccountNumber(MultivaluedMap<String, String> queryParams) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		for (Entry<String, List<String>> entry : queryParams.entrySet()) {
			String key = entry.getKey();
			switch (key) {
			case "initialDate":
			case "finalDate":
				parameters.put(key, Long.parseLong(entry.getValue().get(0)));
				break;
			case "accountNumber":
				if (StringValidator.isValidString(entry.getValue().get(0)))
					parameters.put(key, entry.getValue().get(0));
				break;
			default:
				break;
			}
		}
		return parameters;
	}
	
	/***
	 * This method checks if the given list of movements is empty or not in order to
	 * return an appropriated HTTP response
	 * @param movements	list of movements 
	 * @return	A map of key-valued parameters which contains the appropriated status code and body response
	 */
	private static Map<String, Object> validateMovements(List<Movement> movements){
		Map<String, Object> responseParameters = new HashMap<String, Object>();
		if (movements.isEmpty()) {
			responseParameters.put(STATUS, Response.Status.NOT_FOUND.getStatusCode());
			responseParameters.put(ENTITY, "There are not movements which satisfies the given criteria");
		} else {
			Collections.sort(movements, new Comparator<Movement>() {
				@Override
				public int compare(Movement a, Movement b) {
					return a.getMovementDate().compareTo(b.getMovementDate())
							+ a.getId().compareTo(b.getId());
				}
			});
			responseParameters.put(STATUS, Response.Status.OK.getStatusCode());
			responseParameters.put(ENTITY, gson.toJson(movements));
		}
		return responseParameters;
	}
}
