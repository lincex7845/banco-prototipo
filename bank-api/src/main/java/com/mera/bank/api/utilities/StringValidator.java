/**
 * 
 */
package com.mera.bank.api.utilities;

/**
 * A set of utilities to validate string inputs
 * @author david.mera
 *
 */
public class StringValidator {

	/***
	 * constructor by default
	 */
	private StringValidator(){
		//Constructor
	}
	
	/**
	 * This method checks whether a String is not null and not empty
	 * 
	 * @param string
	 *            The string to check
	 * @return The result of the operation. True if the string is not null or
	 *         empty, otherwise, false.
	 */
	public static boolean isValidString(String string) {

		return (string != null) && (!string.isEmpty());
	}	
}