/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.springframework.dao.DataAccessException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.utilities.StringValidator;
import com.mera.bank.common.entities.Customer;

/**
 * This resources process all HTTP requests related to customers
 * 
 * @author david.mera
 */
@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {

	/**
	 * Instance of {@link com.mera.bank.api.services.BankService}
	 */
	private final BankService bankSpaceService;
	/**
	 *  Used to serialize data to json format
	 */
	private static final Gson gson;
	/**
	 * logger instance
	 */
	private static final Logger LOGGER;

	static {
		LOGGER = Logger.getLogger(CustomerResource.class.getName());
		gson = new GsonBuilder().create();
	}

	/***
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to customers
	 */
	public CustomerResource(BankService bankService) {
		this.bankSpaceService = bankService;
	}

	/***
	 * GET method which returns customer. If it receives a customer's name and
	 * customer's id, returns the customer which satisfies the given criteria,
	 * otherwise, null If it does not receives any criteria, returns all stored
	 * customers
	 * 
	 * @param uriInfo
	 *            Object which represent the URL information associated to the
	 *            HTTP request
	 * @return If it receives a customer's name and customer's id, returns the
	 *         customer which satisfies the given criteria, otherwise, null with
	 *         NOT_FOUND status code. If it does not receives any criteria,
	 *         returns all stored customers If an error occurs during the
	 *         process, INTERNAL_SERVER_ERROR status code is returned
	 */
	@GET
	public Response getCustomers(@Context UriInfo uriInfo) {
		Response response;
		int status = 0;
		String entity = null;
		MultivaluedMap<String, String> queryParams = uriInfo
				.getQueryParameters();
		try {
			if (queryParams.size() > 0) {
				Map<String, String> parameters = validateParameters(queryParams);
				if (parameters.size() == 2) {
					String customerName = parameters.get("customerName");
					String customerDocumentId = parameters
							.get("customerDocumentId");
					Customer customer = bankSpaceService
							.getBankCustomerByIdAndName(customerDocumentId,
									customerName);
					status = Response.Status.OK.getStatusCode();
					entity = gson.toJson(customer);
				} else {
					status = Response.Status.BAD_REQUEST.getStatusCode();
					entity = "It is required sent valid parameters (the customer's name and customer's id) in order to fetch a customer";
				}
			} else {
				List<Customer> customers = bankSpaceService
						.getAllBankCustomers();
				Collections.sort(customers, new Comparator<Customer>() {
					@Override
					public int compare(Customer a, Customer b) {
						return a.getCustomerDocumentId().compareToIgnoreCase(
								b.getCustomerDocumentId())
								+ a.getCustomerName().compareToIgnoreCase(
										b.getCustomerName());
					}
				});
				status = Response.Status.OK.getStatusCode();
				entity = gson.toJson(customers);
			}

		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * POST method which allows to add a customer
	 * 
	 * @param customer
	 *            The customer to be added
	 * @return CREATED status code is returned if the customer was added
	 *         CONFLICT status code is returned if the customer already exists
	 *         into the database BAD_REQUEST status code is returned if the
	 *         passed customer is null or not valid INTERNAL_SERVER_ERROR code
	 *         is returned if an error occurs
	 */
	@POST
	@Path("/add")
	public Response addCustomer(Customer customer) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankCustomer(customer);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The customer was created";
		} catch (org.openspaces.core.EntryAlreadyInSpaceException e) {
			LOGGER.error(e);
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The customer already exists into the database";
		} catch (IllegalArgumentException e) {
			LOGGER.error(e);
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * PUT method which allows to update a certain customer
	 * 
	 * @param customerDocumentId
	 *            The customer's identified
	 * @param customerName
	 *            The customer's name to set
	 * @return OK status code is returned if the customer was updated NOT_FOUND
	 *         status code is returned if the customer does not exist into the
	 *         database BAD_REQUEST status code is returned if the passed
	 *         customer is null or not valid INTERNAL_SERVER_ERROR code is
	 *         returned if an error occurs during the process
	 */
	@PUT
	@Path("/update/{customerDocumentId}")
	public Response updateCustomer(
			@PathParam("customerDocumentId") String customerDocumentId,
			String customerName) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.updateBankCustomer(customerDocumentId,
					new Customer(customerName, customerDocumentId, null));
			status = Response.Status.OK.getStatusCode();
			entity = "The customer was updated";
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * DELETE method which allows to delete a customer
	 * 
	 * @param customerDocumentId
	 *            The customer's identifier
	 * @return OK status code is returned if the customer was deleted NOT_FOUND
	 *         status code is returned if the customer does not exist into the
	 *         database INTERNAL_SERVER_ERROR code is returned if an error
	 *         occurs during the process
	 */
	@DELETE
	@Path("/delete/{customerDocumentId}")
	public Response updateCustomer(
			@PathParam("customerDocumentId") String customerDocumentId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.deleteBankCustomer(customerDocumentId);
			status = Response.Status.OK.getStatusCode();
			entity = "The customer was deleted";
		} catch (NoSuchElementException e) {
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * This method checks if the passed parameters are valid
	 * 
	 * @param queryParams
	 *            A map of parameters which represents the query parameters in
	 *            the URL
	 * @return A map of key-valued parameters. It can be empty if the checked
	 *         parameters are not valid
	 */
	private static Map<String, String> validateParameters(
			MultivaluedMap<String, String> queryParams) {
		Map<String, String> parameters = new HashMap<String, String>();
		for (Entry<String, List<String>> entry : queryParams.entrySet()) {
			String key = entry.getKey();
			switch (key) {
			case "customerName":
			case "customerDocumentId":
				if (StringValidator.isValidString(entry.getValue().get(0)))
					parameters.put(key, entry.getValue().get(0));
				break;
			default:
				break;
			}
		}
		return parameters;
	}
}
