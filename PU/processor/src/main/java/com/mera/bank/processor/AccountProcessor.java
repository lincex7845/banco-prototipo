package com.mera.bank.processor;

import java.math.BigDecimal;
import org.jboss.logging.Logger;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Account;

/***
 * This object process account POJOS which 
 * "is processed" flags are equals to false
 * @author david.mera
 *
 */
public class AccountProcessor {

	/**
	 * Logger object
	 */
	private static final Logger LOGGER = Logger.getLogger(AccountProcessor.class.getName());

	/***
	 * The constructor by default
	 */
	public AccountProcessor() {
		//Used by spring in order to initialize this processor
	}

	/***
	 * This method process bank accounts by setting the flag "is processed" in true
	 * @param account	The account to be processed
	 * @return The processed account
	 */
	public Account saveAccount(Account account) {
		LOGGER.info("Incomming Account: " + new Gson().toJson(account));
		if (!account.getIsProcessed()) {
			BigDecimal money = account.getAccountBalance() == null ? new BigDecimal(0) : account.getAccountBalance();
			account.setAccountBalance(money);
			account.setIsProcessed(Boolean.TRUE);
			LOGGER.info("PROCESSED Account: " + account.toString());
		}
		return account;
	}
}
