package com.mera.bank.processor;

import org.jboss.logging.Logger;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Customer;

/***
 * This object process bank customers which "is processed" flags are equals to false
 * @author david.mera
 *
 */
public class CustomerProcessor {

	/***
	 * The logger object
	 */
	private static final Logger LOGGER = Logger.getLogger(CustomerProcessor.class.getName());

	/***
	 * The constructor by default
	 */
	public CustomerProcessor() {
		//Used by spring in order to initialize
	}

	/**
	 * This method process a bank customer by setting the "is processed" flag in true
	 * @param customer	The customer to be processed
	 * @return	The processed customer
	 */
	public Customer saveCustomer(Customer customer) {
		LOGGER.info("Incoming customer: " + new Gson().toJson(customer));
		if (!customer.getIsProcessed()) {
			customer.setIsProcessed(Boolean.TRUE);
			LOGGER.info("PROCESSED Customer: " + customer.toString());
		}
		return customer;
	}
}
