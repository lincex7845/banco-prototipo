package com.mera.bank.processor;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Test;
import com.mera.bank.common.entities.Account;

public class AccountProcessorTest {

	@Test
	public void testAddAccount() {
		Account bankAccount = new Account(null, "07000-500", new BigDecimal(0),
				"12969860", Boolean.FALSE);
		AccountProcessor accountProcessor = new AccountProcessor();
		Account bankAccountProcessed1 = accountProcessor
				.saveAccount(bankAccount);
		assertEquals("Account is not processed.", Boolean.TRUE,
				bankAccountProcessed1.getIsProcessed());
		assertEquals("Processed account mismatch.",
				bankAccount.getAccountNumber(),
				bankAccountProcessed1.getAccountNumber());
	}

}
