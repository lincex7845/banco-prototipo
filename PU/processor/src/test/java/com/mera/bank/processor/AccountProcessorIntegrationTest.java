package com.mera.bank.processor;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import org.jboss.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Account;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:com/mera/bank/processor/AccountProcessorIntegrationTest-context.xml")
public class AccountProcessorIntegrationTest {

	@Autowired
	protected GigaSpace gigaSpace;

	@Before
	public void setUpBefore() throws Exception {
		gigaSpace.clear(null);
	}

	@After
	public void tearDownAfter() throws Exception {
		gigaSpace.clear(null);
	}

	protected String[] getConfigLocations() {
		return new String[] { "com/mera/bank/processor/AccountProcessorIntegrationTest-context.xml" };
	}

	@Test
	public void testSaveAccount() throws InterruptedException {
		Account bankAccount = new Account(null, "07000-500", new BigDecimal(0),
				"12969860", Boolean.FALSE);
		Logger logger = Logger.getLogger(AccountProcessorIntegrationTest.class
				.getName());
		logger.info("Account to write: " + new Gson().toJson(bankAccount));
		gigaSpace.write(bankAccount);
		Thread.sleep(300);
		// ** template
		Account template = new Account();
		template.setAccountNumber("07000-500");
		template.setIsProcessed(Boolean.TRUE);
		// ** verify
		Account processed = (Account) gigaSpace.read(template, 500);
		assertEquals("Account is not processed.", Boolean.TRUE,
				processed.getIsProcessed());
		assertEquals("Processed account mismatch.",
				bankAccount.getAccountNumber(), processed.getAccountNumber());
	}

}
