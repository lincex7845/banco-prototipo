package com.mera.bank.processor;

import static org.junit.Assert.*;

//import java.util.HashSet;
//import java.util.Set;
import org.jboss.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.Gson;
//import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:com/mera/bank/processor/CustomerProcessorIntegrationTest-context.xml")
public class CustomerProcessorIntegrationTest {

	@Autowired
	protected GigaSpace gigaSpace;

	@Before
	public void setUpBeforeClass() throws Exception {
		gigaSpace.clear(null);
	}

	@After
	public void tearDownAfterClass() throws Exception {
		gigaSpace.clear(null);
	}

	protected String[] getConfigLocations() {
		return new String[] { "com/mera/bank/processor/CustomerProcessorIntegrationTest-context.xml" };
	}

	@Test
	public void testSaveCustomer() throws InterruptedException {
		Customer cu = new Customer();
		cu.setCustomerDocumentId("89042751643");
		cu.setCustomerName("David Mera");
		cu.setIsProcessed(Boolean.FALSE);
		gigaSpace.write(cu);
		Logger logger = Logger.getLogger(CustomerProcessorIntegrationTest.class
				.getName());
		logger.info("Customer writed: " + new Gson().toJson(cu));
		Thread.sleep(300);
		// //** template
		Customer template = new Customer();
		template.setCustomerDocumentId("89042751643");
		template.setIsProcessed(Boolean.TRUE);
		// ** verify
		Customer processed = (Customer) gigaSpace.read(template);
		String expectedCustomer = String.format("Name: %s - ID: %s",
				cu.getCustomerName() + " ", cu.getCustomerDocumentId());
		String processedCustomer = String.format("Name: %s - ID: %s",
				processed.getCustomerName() + " ",
				processed.getCustomerDocumentId());
		assertEquals("Processed Flag is false, data was not processed",
				Boolean.TRUE, processed.getIsProcessed());
		assertEquals("Processed mismatch", expectedCustomer, processedCustomer);
	}
}