package com.mera.bank.processor;

import static org.junit.Assert.*;
import org.junit.Test;
import com.mera.bank.common.entities.Customer;

public class CustomerProcessorTest {

	@Test
	public void testSaveOrUpdateCustomer() {
		CustomerProcessor processor = new CustomerProcessor();
		Customer cu = new Customer();
		cu.setCustomerDocumentId("89042751643");
		cu.setCustomerName("David Mera");
		cu.setIsProcessed(Boolean.FALSE);
		Customer processed = processor.saveCustomer(cu);
		String expectedCustomer = String.format("Name: %s - ID: %s",
				cu.getCustomerName() + " ", cu.getCustomerDocumentId());
		String processedCustomer = String.format("Name: %s - ID: %s",
				processed.getCustomerName() + " ",
				processed.getCustomerDocumentId());
		assertEquals("Processed Flag is false, data was not processed",
				Boolean.TRUE, processed.getIsProcessed());
		assertEquals("Processed mismatch", expectedCustomer, processedCustomer);
	}

}
