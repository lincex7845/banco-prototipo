/**
 * 
 */
package com.mera.bank.processor;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;
import org.jboss.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Movement;
import com.mera.bank.common.enumerations.MovementType;

/**
 * @author david.mera
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:com/mera/bank/processor/TransactionProcessorIntegrationTest-context.xml")
public class TransactionProcessorIntegrationTest {

	@Autowired
	protected GigaSpace gigaSpace;

	private Logger logger = Logger
			.getLogger(TransactionProcessorIntegrationTest.class.getName());

	@Before
	public void setUpBefore() throws Exception {
		gigaSpace.clear(null);
	}

	@After
	public void tearDownAfter() throws Exception {
		gigaSpace.clear(null);
	}

	protected String[] getConfigLocations() {
		return new String[] { "com/mera/bank/processor/TransactionProcessorIntegrationTest-context.xml" };
	}

	@Test
	public void testProcessTransaction() throws InterruptedException {
		String accountNumber = "07000-500";
		// *** Create account
		Account bankAccount = new Account(null, accountNumber,
				new BigDecimal(0), "12969860", Boolean.FALSE);
		logger.info("Account to write: " + new Gson().toJson(bankAccount));
		gigaSpace.write(bankAccount);
		Thread.sleep(100);
		// *** Verify if the bank accout was processed
		Account accountTemplate = new Account();
		accountTemplate.setAccountNumber(accountNumber);
		accountTemplate.setIsProcessed(Boolean.TRUE);
		Account processedAccount1 = (Account) gigaSpace.read(accountTemplate,
				200);
		// *** Create Transaction 1
		Movement newMovement = new Movement(null, new Date(), new BigDecimal(
				5000000), MovementType.DEBIT, accountNumber, Boolean.FALSE);
		logger.info("Transaction 1: " + new Gson().toJson(newMovement));
		gigaSpace.write(newMovement);
		// Thread.sleep(50);
		// *** Create Transaction 2
		Movement newMovement2 = new Movement(null, new Date(), new BigDecimal(
				1000000), MovementType.CREDIT, accountNumber, Boolean.FALSE);
		logger.info("Transaction 2: " + new Gson().toJson(newMovement2));
		gigaSpace.write(newMovement2);
		Thread.sleep(50);
		// *** Verify if the bank account was processed again
		Account processedAccount2 = (Account) gigaSpace.read(accountTemplate,
				200);
		// *** Verify if the bank transaction was processed
		Movement movementTemplate = new Movement();
		movementTemplate.setAccountNumber(accountNumber);
		movementTemplate.setIsProcessed(true);
		Movement bankTransactionProcessed = (Movement) gigaSpace.read(
				movementTemplate, 200);
		// ***
		assertEquals("Account is not processed.", Boolean.TRUE,
				processedAccount1.getIsProcessed());
		assertEquals("Account is not processed.", Boolean.TRUE,
				processedAccount2.getIsProcessed());
		assertEquals("Processed account mismatch.", accountNumber,
				processedAccount1.getAccountNumber());
		assertEquals("Processed account mismatch.", accountNumber,
				processedAccount2.getAccountNumber());
		assertEquals("Processed account balance is wrong",
				processedAccount2.getAccountBalance(), new BigDecimal(
						5000000L - 1000000L));
		assertEquals("Transaction is not processed.", Boolean.TRUE,
				bankTransactionProcessed.getIsProcessed());
	}
}
