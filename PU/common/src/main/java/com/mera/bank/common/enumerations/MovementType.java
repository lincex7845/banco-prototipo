package com.mera.bank.common.enumerations;

/**
 * @author david.mera
 * This enumeration represents the types of a movement.
 */
public enum MovementType {

	/**
	 * When a amount of money is placed into the account
	 */
	DEBIT("DEBIT") ,
	/**
	 * When a amount of money is taken from the account
	 */
	CREDIT("CREDIT");
	
	/**
	 * Word which represents the financial movement
	 */
	private final String type;
	
	/**
	 * Constructor
	 * @param type	Word which represents the financial movement
	 */
	private MovementType(String type)
	{
		this.type = type;
	}
	
	/***
	 * @return The kind of movement in words
	 */
	public String toString()
	{
		return this.type;
	}
}
