package com.mera.bank.common.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;
import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;


/**
 * @author david.mera
 * This entity represents the information associated to a customer
 */
@Entity
@Table(name="customers")
@SpaceClass(persist=true)
public class Customer {
	
	/***
	 * The customer's name	 * 
	 */
	@NotEmpty
	private String customerName;
		
	/***
	 * Alphanumeric string that identifies the customer 
	 */
	@Id	
	@NotEmpty
	private String customerDocumentId;
		
	private Boolean isProcessed;
	
	/***
	 * The constructor by default
	 */
	public Customer(){}
	
	/***
	 * The parameterized constructor
	 * @param customerName				Customer's Name
	 * @param customerDocumentId		Customer's ID
	 * @param isProcessed				This flags indicates if the customer was processed
	 */
	public Customer(
			 String customerName,String customerDocumentId	
			 ,Boolean isProcessed
			)
	{
		this.setCustomerName(customerName);
		this.setCustomerDocumentId(customerDocumentId);
		this.isProcessed = isProcessed;
	}
	
	@Override
	/**
	 * Return de customer's identifier
	 */
	public String toString()
	{
		return this.customerDocumentId;
	}
	
	@Override
	public boolean equals(Object obj)
	{			
		boolean nulo = this == null && obj == null;
		if(nulo)
			return true;
		else
		{
			if(obj.getClass() != this.getClass())
				return false;
			Customer other = (Customer)obj;			
			if(!this.getCustomerDocumentId().equals(other.getCustomerDocumentId()))
				return false;			
			if( !this.getCustomerName().equalsIgnoreCase(other.getCustomerName()))
				return false;	
			if(this.hashCode() != other.hashCode())
				return false;
			return true;
		}
	}
	
	@Override
	public int hashCode()
	{
		int hashCode = 7;		
		hashCode = hashCode * 49 + this.getCustomerDocumentId().hashCode();
		hashCode = hashCode * 49 + this.getCustomerName().hashCode();
		return hashCode;
	}
	
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
			
	/**
	 * @return the customerDocumentId
	 */
	@SpaceRouting
	@SpaceId(autoGenerate=false)
	public String getCustomerDocumentId() {
		return customerDocumentId;
	}
	
	/**
	 * @return the processed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}	
	
	/**
	 * @param customerDocumentId the customerDocumentId to set
	 */
	public void setCustomerDocumentId(String customerDocumentId) {
		this.customerDocumentId = customerDocumentId;
	}
	
	/**
	 * @param processed the processed to set
	 */
	public void setIsProcessed(Boolean processed) {
		this.isProcessed = processed;
	}	
}
