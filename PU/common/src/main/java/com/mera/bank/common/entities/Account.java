package com.mera.bank.common.entities;

import java.io.Serializable;
import java.math.BigDecimal;
//import java.util.Set;
//import com.mera.bank.common.entities.Movement;

import javax.persistence.Column;
//import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToMany;
import javax.persistence.Table;
//import javax.persistence.Transient;
import javax.validation.constraints.Min;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;

/**
 * @author david.mera This entity represents the information associated to an
 *         account bank
 */
@Entity
@Table(name = "Accounts")
@SpaceClass
public class Account implements Serializable {

	private static final long serialVersionUID = -6197735448432617430L;

	/***
	 * Account Identifier on the database
	 */
	@Id
	private String id;

	/***
	 * Account Identifier on business transactions
	 */
	@Column(unique = true)
	private String accountNumber;

	/***
	 * The amount of money. It increases when a debit is performed. On the other
	 * hand, it decreases when a credit is performed.
	 */
	@Min(0)
	private BigDecimal accountBalance;

	/***
	 * A set of credits or debits which changes the account balance.
	 */
	// @OneToMany(cascade=CascadeType.ALL)
	// @Transient
	// private Set<Movement> accountMovements;

	private String customerDocumentId;

	private Boolean isProcessed;

	/***
	 * Default constructor
	 */
	public Account() {
	}

	/***
	 * Parametrized constructor
	 * 
	 * @param id
	 *            Number that identifies the account on the database
	 * @param number
	 *            Alphanumeric string which identifies the account on business
	 *            transactions
	 * @param value
	 *            The amount of money which the account has
	 * @param customerDocumentId
	 *            The customer's identifier
	 */
	public Account(String id, String number, BigDecimal value,
	// , Set<Movement> movements
			String customerDocumentId, Boolean isProcessed) {
		this.setId(id);
		this.setAccountNumber(number);
		this.setAccountBalance(value);
		// this.setAccountMovements(movements);
		this.setCustomerDocumentId(customerDocumentId);
		this.isProcessed = isProcessed;
	}

	/**
	 * @return the id
	 */
	@SpaceRouting
	@SpaceId(autoGenerate = true)
	public String getId() {
		return id;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the accountBalance
	 */
	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	/**
	 * @return the processed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @param accountNumber
	 *            the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @param accountBalance
	 *            the accountBalance to set
	 */
	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	/**
	 * @return the accountMovements
	 */
	// public Set<Movement> getAccountMovements() {
	// return accountMovements;
	// }

	/**
	 * @param accountMovements
	 *            the accountMovements to set
	 */
	// public void setAccountMovements(Set<Movement> accountMovements) {
	// this.accountMovements = accountMovements;
	// }

	/**
	 * @param processed
	 *            the processed to set
	 */
	public void setIsProcessed(Boolean processed) {
		this.isProcessed = processed;
	}

	public String toString() {
		return this.accountNumber;
	}

	/**
	 * @return the customerDocumentId
	 */
	public String getCustomerDocumentId() {
		return customerDocumentId;
	}

	/**
	 * @param customerDocumentId
	 *            the customerDocumentId to set
	 */
	public void setCustomerDocumentId(String customerDocumentId) {
		this.customerDocumentId = customerDocumentId;
	}
}
