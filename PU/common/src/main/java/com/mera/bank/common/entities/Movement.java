package com.mera.bank.common.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceRouting;
import com.mera.bank.common.enumerations.MovementType;

/***
 * Entity which represents information about a bank account movement. The
 * movement could be a credit movement or a debit movement.
 * 
 * @author david.mera
 *
 */
@Entity
@Table(name = "Movements")
@SpaceClass
public class Movement implements Serializable {

	private static final long serialVersionUID = -3577655737164811762L;

	/***
	 * The movement's identifier on the database
	 */
	private String id;

	/***
	 * Represents when the movement was madden
	 */
	private Date movementDate;

	/***
	 * The ammount of money
	 */
	@Min(1)
	private BigDecimal movementValue;

	/***
	 * The kind of movement. It could be a credit movement or a debit movement
	 */
	private MovementType movementType;

	private String accountNumber;

	private Boolean isProcessed;

	/***
	 * The constructor by default
	 */
	public Movement() {
	}
	
	/***
	 * The parameterized constructor
	 * @param id			The movement's identifier
	 * @param movementDate	The movement's date
	 * @param movementValue	The movement's amount of money
	 * @param movementType	The movement's type
	 * @param accountNumber	The bank account to be updated
	 * @param isProcessed	It indicates if the movement has been processed
	 */
	public Movement(String id, Date movementDate, BigDecimal movementValue,
			MovementType movementType, String accountNumber, Boolean isProcessed) {
		this.setId(id);
		this.setMovementDate(movementDate);
		this.setMovementValue(movementValue);
		this.setMovementType(movementType);
		this.setAccountNumber(accountNumber);
		this.setIsProcessed(isProcessed);
	}
	

	/**
	 * @return the id
	 */
	@Id
	@SpaceRouting
	@SpaceId(autoGenerate = true)
	public String getId() {
		return id;
	}

	/**
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}

	/**
	 * @return the movementValue
	 */
	public BigDecimal getMovementValue() {
		return movementValue;
	}

	/**
	 * @return the movementType
	 */
	public MovementType getMovementType() {
		return movementType;
	}

	/**
	 * @return the processed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param movementDate
	 *            the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}

	/**
	 * @param movementValue
	 *            the movementValue to set
	 */
	public void setMovementValue(BigDecimal movementValue) {
		this.movementValue = movementValue;
	}

	/**
	 * @param movementType
	 *            the movementType to set
	 */
	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}

	/**
	 * @param processed
	 *            the processed to set
	 */
	public void setIsProcessed(Boolean processed) {
		this.isProcessed = processed;
	}

	/**
	 * @param accountNumber
	 *            the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}