/**
 * 
 */
package com.mera.bank.processor;

import java.math.BigDecimal;
//import java.util.HashSet;
//import java.util.Set;

import org.jboss.logging.Logger;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;

import com.gigaspaces.client.WriteModifiers;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Movement;
import com.mera.bank.common.enumerations.MovementType;

/**
 * @author david.mera
 *
 */
public class TransactionProcessor {

	@Autowired
	private GigaSpace gigaSpace;
	private Logger logger = Logger.getLogger(TransactionProcessor.class.getName());

	public TransactionProcessor() {

	}

	public Movement processTransaction(Movement newMovement) {
		logger.info("Incoming transaction: " + new Gson().toJson(newMovement));
		if (newMovement == null) {
			logger.error("Null movements are not allowed");
		}

		Account accountTemplate = new Account();
		accountTemplate.setAccountNumber(newMovement.getAccountNumber());
		if (!newMovement.getIsProcessed()) {
			Account bankAccount = (Account) gigaSpace.read(accountTemplate, 500);
			if (bankAccount == null) {
				logger.warn("The bank account " + newMovement.getAccountNumber() + " is not registred yet");
			} else {
				BigDecimal money = bankAccount.getAccountBalance() == null ? new BigDecimal(0)
						: bankAccount.getAccountBalance();
//				Set<Movement> bankAccountMovements = bankAccount.getAccountMovements() == null ? new HashSet<Movement>()
//						: bankAccount.getAccountMovements();
				BigDecimal balance = newMovement.getMovementType() == MovementType.DEBIT
						? money.add(newMovement.getMovementValue()) : money.subtract(newMovement.getMovementValue());
				if (balance.compareTo(BigDecimal.ZERO) == -1)
				// throw new UnsupportedOperationException("Negative balance is
				// not allowed in bank accounts.");
				{
					newMovement.setIsProcessed(Boolean.TRUE);
					logger.error("Negative balance is not allowed in bank accounts.");
				} else {
					bankAccount.setAccountBalance(balance);
					newMovement.setIsProcessed(Boolean.TRUE);
				}
//				bankAccountMovements.add(newMovement);
//				bankAccount.setAccountMovements(bankAccountMovements);
				bankAccount.setIsProcessed(Boolean.FALSE);
				gigaSpace.write(bankAccount, WriteModifiers.PARTIAL_UPDATE);
			}
		}
		newMovement.setIsProcessed(Boolean.TRUE);
		logger.info("PROCESSED Transaction: " + newMovement.getId());
		return newMovement;
	}
}
