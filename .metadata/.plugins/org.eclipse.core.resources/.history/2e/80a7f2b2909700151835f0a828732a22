/**
 * 
 */
package com.mera.bank.api;

import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.UrlSpaceConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import com.mera.bank.api.resources.CustomerResource;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.services.BankSpaceServiceImpl;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * @author DavidCamilo
 *
 */
public class BankApiApplication extends Application<BankApiConfiguration> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.dropwizard.Application#initialize(io.dropwizard.setup.Bootstrap)
	 */
	@Override
	public void initialize(Bootstrap<BankApiConfiguration> arg0) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration,
	 * io.dropwizard.setup.Environment)
	 */
	@Override
	public void run(BankApiConfiguration configuration, Environment environment) throws Exception {

		UrlSpaceConfigurer configurer = new UrlSpaceConfigurer(configuration.getGigaSpaceConfiguration().getSpaceUrl())
				.lookupTimeout(2000);
		GigaSpace gigaSpace = new GigaSpaceConfigurer(configurer).create();
		BankSpaceServiceImpl bankService = new BankSpaceServiceImpl(gigaSpace);
		final CustomerResource customerService = new CustomerResource(gigaSpace, bankService);
		// register resources
		environment.jersey().register(customerService);

		// TO-DO healthcheck
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new BankApiApplication().run(args);
	}

	@Override
	public String getName() {
		return "bank-api";
	}
}
