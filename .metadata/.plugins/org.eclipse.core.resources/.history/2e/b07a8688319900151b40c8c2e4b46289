package com.mera.bank.processor;

import java.math.BigDecimal;
import org.jboss.logging.Logger;
import org.openspaces.core.GigaSpace;
import org.openspaces.core.transaction.manager.DistributedJiniTxManagerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.gigaspaces.client.WriteModifiers;
import com.google.gson.Gson;
import com.j_spaces.core.client.SQLQuery;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Movement;
import com.mera.bank.common.enumerations.MovementType;

/**
 * This object process bank transactions (movements) in order to update the
 * balance in bank accounts
 * 
 * @author david.mera
 *
 */
public class TransactionProcessor {

	/**
	 * instance of bank's space
	 */
	@Autowired
	private GigaSpace gigaSpace;

	/***
	 * The logger object
	 */
	private static final Logger LOGGER = Logger.getLogger(TransactionProcessor.class.getName());

	public void setGigaSpace(GigaSpace gigaSpace) {
		this.gigaSpace = gigaSpace;
	}

	/***
	 * The constructor by default
	 */
	public TransactionProcessor() {
		// Used by spring to initialize PU
	}

	/**
	 * This method process a bank transaction (movement) by updating the balance
	 * into the bank account requested by the transaction itself. Negative
	 * balance are not be settled by this method
	 * 
	 * @param newMovement
	 *            The movement to be processed
	 * @return The processed movement
	 * @throws Exception
	 */
	public Movement processTransaction(Movement newMovement) {
		PlatformTransactionManager ptm = null;
		TransactionStatus status = null;
		try {
			ptm = new DistributedJiniTxManagerConfigurer().transactionManager();
			DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
			definition.setPropagationBehavior(Propagation.REQUIRES_NEW.ordinal());
			definition.setIsolationLevel(DefaultTransactionDefinition.ISOLATION_READ_COMMITTED);
			status = ptm.getTransaction(definition);
			LOGGER.info("Incoming transaction: " + new Gson().toJson(newMovement));
			if (newMovement == null) {
				LOGGER.error("Null movements are not allowed");
			} else {
				Account accountTemplate = new Account();
				accountTemplate.setAccountNumber(newMovement.getAccountNumber());
				accountTemplate.setIsProcessed(Boolean.TRUE);
				if (!newMovement.getIsProcessed()) {
					Account bankAccount = (Account) gigaSpace.read(accountTemplate, 500);
					if (bankAccount == null) {
						LOGGER.warn("The bank account " + newMovement.getAccountNumber() + " is not registred yet");
					} else {
						makeMovement(bankAccount, newMovement);
					}
					newMovement.setIsProcessed(Boolean.TRUE);
					ptm.commit(status);
					
				}
				LOGGER.info("PROCESSED Transaction: " + newMovement.getId());
			}
		}
		catch(org.springframework.transaction.TransactionException e){
			ptm.rollback(status);
		}
		catch (Exception e1) {
			LOGGER.error(e1);
		}
		return newMovement;
	}

	private void makeMovement(Account bankAccount, Movement newMovement) {
		BigDecimal money = bankAccount.getAccountBalance() == null ? new BigDecimal(0)
				: bankAccount.getAccountBalance();
		BigDecimal balance = newMovement.getMovementType() == MovementType.DEBIT
				? money.add(newMovement.getMovementValue()) : money.subtract(newMovement.getMovementValue());
		// This line check if the new balance will be less than
		// zero.
		// Negative balance is not allowed
		if (balance.compareTo(BigDecimal.ZERO) < 0) {
			LOGGER.error("Negative balance is not allowed in bank accounts.");
		} else {
			// The new value for the balance is settled on the account
			bankAccount.setAccountBalance(balance);
		}
		// The bank account is updated on the space
		gigaSpace.write(bankAccount, WriteModifiers.PARTIAL_UPDATE);
	}
}
