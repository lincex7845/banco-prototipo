/**
 * 
 */
package com.mera.bank.webApp.services;

import static java.util.Arrays.asList;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.collections4.Closure;
import org.apache.commons.collections4.CollectionUtils;
import org.jboss.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.webApp.models.MovementModel;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * @author david.mera
 *
 */
public class MovementService {

	private static Gson gson;
	private static Client client;
	private static final String SERVICEHOST;
	private static final Logger LOGGER;
	private static final String ZONACOLOMBIA;

	static {
		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
		client = Client.create();
		SERVICEHOST = "http://localhost:9000";
		LOGGER = Logger.getLogger(AccountService.class.getName());
		ZONACOLOMBIA = "America/Bogota";
	}

	private MovementService() {

	}

	public static ClientResponse addMovement(MovementModel movement) {
		ClientResponse response = null;
		movement.setMovementDate(new DateTime().withZone(DateTimeZone.forID(ZONACOLOMBIA)).toDate());
		movement.setIsProcessed(Boolean.FALSE);
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/movements/add");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				gson.toJson(movement));
		LOGGER.info("Request for adding the movement: " + String.format("%s - %s - %s - %s", movement.getAccountNumber(), movement.getMovementType(),
				movement.getMovementValue().toString(), movement.getMovementDate().toString()));
		try {
			response = res.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to add account: ", e);
		}
		return response;
	}

	public static MovementModel[] getMovementsByDateAndAccountNumber(Date initialDate, Date finalDate,
			String accountNumber) {
		MovementModel[] movements = new MovementModel[0];
		try {
			AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/movements");
			MultivaluedMap<String, String> parameters = new MultivaluedMapImpl();
			parameters.put("initialDate", asList(String.valueOf(new DateTime(initialDate).withZone(DateTimeZone.forID(ZONACOLOMBIA)).getMillis())));
			parameters.put("finalDate", asList(String.valueOf(new DateTime(finalDate).withZone(DateTimeZone.forID(ZONACOLOMBIA)).getMillis())));
			parameters.put("accountNumber", asList(accountNumber));
			Future<ClientResponse> res = awr.queryParams(parameters).type(MediaType.APPLICATION_JSON_TYPE)
					.get(ClientResponse.class);
			LOGGER.info("Request for getting movements by account number " + accountNumber + ", initial Date "
					+ initialDate.toString() + ", final Date " + finalDate.toString());
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				ArrayList<MovementModel> movementsList = new ArrayList<MovementModel>();
				movementsList.addAll(Arrays.asList(gson.fromJson(entity, MovementModel[].class)));
				CollectionUtils.forAllDo(movementsList, new Closure<MovementModel>()  
				  { 
					@Override
					public void execute(MovementModel movement) {
						Date fixedDate = new DateTime(movement.getMovementDate()).withZone(DateTimeZone.forID(ZONACOLOMBIA)).toDate();
						movement.setMovementDate(fixedDate);
					}  
				  });  
				movements = movementsList.toArray(new MovementModel[movementsList.size()]);
			} else {
				LOGGER.error(String.format("Failed attempt to get accounts. HTTP %d - %s", status, entity));
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to get accounts: ", e);
		}
		return movements;
	}
}
