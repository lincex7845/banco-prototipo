/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hibernate.validator.constraints.NotEmpty;
import org.jboss.logging.Logger;
import org.springframework.dao.DataAccessException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.common.entities.Account;

/**
 * @author david.mera This resources process all HTTP requests related to
 *         accounts
 */
@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

	private final BankService bankSpaceService;
	private final Gson gson;
	private final Logger logger;

	/**
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to accounts
	 */
	public AccountResource(BankService bankService) {
		this.bankSpaceService = bankService;
		gson = new GsonBuilder().create();
		logger = Logger.getLogger(AccountResource.class.getName());
	}

	/***
	 * GET method which allows to get account by a given Id
	 * 
	 * @param accountId
	 *            The account's Id
	 * @return OK status code if any account is identified by the given id.
	 *         NOT_FOUND if the given id does not exists into the
	 *         database. INTERNAL_SERVER_ERROR if an error occurs during the
	 *         process
	 */
	@GET
	public Response getBankAccountById(@QueryParam("accountId") @NotEmpty String accountId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			Account account = this.bankSpaceService.getAccountById(accountId);
			status = Response.Status.OK.getStatusCode();
			entity = gson.toJson(account);
		} catch (NoSuchElementException e) {
			logger.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (org.springframework.dao.DataAccessException e) {
			logger.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			logger.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get account by a given number
	 * 
	 * @param accountNumber
	 *            The account's number
	 * @return OK status code if any account is identified by the given number.
	 *         NOT_FOUND if the given number does not exists into the
	 *         database. INTERNAL_SERVER_ERROR if an error occurs during the
	 *         process
	 */
	@GET
	public Response getBankAccountByNumber(@QueryParam("accountNumber") @NotEmpty String accountNumber) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			Account account = this.bankSpaceService.getAccountByNumber(accountNumber);
			status = Response.Status.OK.getStatusCode();
			entity = gson.toJson(account);
		} catch (NoSuchElementException e) {
			logger.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (org.springframework.dao.DataAccessException e) {
			logger.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			logger.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * GET method which allows to get account by a given customer's id
	 * 
	 * @param customerDocumentId
	 *            The customer's Id
	 * @return OK status code if any account is identified by the given
	 *         customer's id. NOT_FOUND exception if the given customer's id does
	 *         not exists into the database. INTERNAL_SERVER_ERROR if an error
	 *         occurs during the process
	 */
	@GET
	public Response getBankAccountsByCustomerId(@QueryParam("customerDocumentId") @NotEmpty String customerDocumentId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			List<Account> accounts = this.bankSpaceService.getBankAccountsByCustomerId(customerDocumentId);
			if (accounts.isEmpty()) {
				status = Response.Status.NOT_FOUND.getStatusCode();
				entity = "There are not accounts associated to customer " + customerDocumentId;
			} else {
				Collections.sort(accounts, new Comparator<Account>() {
					@Override
					public int compare(Account a, Account b) {
						return a.getId().compareTo(b.getId());
					}
				});
				status = Response.Status.OK.getStatusCode();
				entity = gson.toJson(accounts);
			}
		} catch (org.springframework.dao.DataAccessException e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * POST method that allows to create a new bank account
	 * 
	 * @param account
	 *            The new bank account to create
	 * 
	 * @return CREATED status code if the account was created. CONFLICT status
	 *         code if the account already exists into the database. BAD_REQUEST
	 *         status code if the passed account is null or not valid.
	 *         INTERNAL_SERVER_ERROR if an error occurs during the process
	 */
	@POST
	@Path("/add")
	public Response addBankAccount(Account account) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankAccount(account);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The account was created";
		} catch (org.openspaces.core.EntryAlreadyInSpaceException e) {
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The account already exists into the database";
		} catch (IllegalArgumentException e) {
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/***
	 * DELETE method created in order to delete bank accounts
	 * @param accountId	The account's identifier
	 * @return
	 * 		OK status code is returned if the account was deleted
	 * 		NOT_FOUND status code is returned if the account does not exist into the database
	 * 		INTERNAL_SERVER_ERROR code is returned if an error occurs during the process
	 */
	@DELETE
	@Path("/delete/{accountId}")
	public Response updateCustomer(@PathParam("accountId") String accountId) {
		Response response;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.deleteBankAccount(accountId);
			status = Response.Status.OK.getStatusCode();
			entity = "The account was deleted";
		} catch (NoSuchElementException e) {
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}
}
