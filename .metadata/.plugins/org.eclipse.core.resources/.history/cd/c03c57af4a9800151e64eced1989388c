/**
 * 
 */
package com.mera.bank.webApp.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.mera.bank.webApp.models.CustomerModel;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.jboss.logging.Logger;

/**
 * @author david.mera
 *
 */
public class BankService {

	private static Gson gson;
	private static Client client;
	private static final String SERVICEHOST;
	private static final Logger LOGGER;

	static {
		gson = new Gson();
		client = Client.create();
		SERVICEHOST = "http://localhost:9000";
		LOGGER = Logger.getLogger(BankService.class.getName());
	}

	private BankService() {

	}

	public static ClientResponse addBankCustomer(CustomerModel customerModel) {
		ClientResponse response = null;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/add");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				gson.toJson(customerModel));
		LOGGER.info("Customer to add: "
				+ String.format("%s - %s", customerModel.getCustomerDocumentId(), customerModel.getCustomerName()));
		try {
			response = res.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to add customer: ", e);
		}
		return response;
	}

	public static CustomerModel[] getBankCustomers() {
		CustomerModel[] customers = new CustomerModel[0];
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		LOGGER.info("Request for getting customers");
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				customers = gson.fromJson(entity, CustomerModel[].class);
			} else {
				LOGGER.error(String.format("Failed attempt to get customers. HTTP %d - %s", status, entity));
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to get customers: ", e);
		}
		return customers;
	}

	public static boolean updateBankCustomer(String customerDocumentId, String customerNewName) {
		boolean result = false;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/update/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class,
				customerNewName);
		LOGGER.info("Request for updating customer " + customerDocumentId);
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			switch (status) {
			case HttpServletResponse.SC_OK:
				result = true;
				break;
			default:
				LOGGER.error(String.format("Failed attempt to update customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
				break;
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to update a customer: ", e);
		}
		return result;
	}

	public static boolean deleteBankCustomer(String customerDocumentId) {
		boolean result = false;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/delete/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
		LOGGER.info("Request for deleting customer " + customerDocumentId);
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				result = true;
				}
			else{
				LOGGER.error(String.format("Failed attempt to delete customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to delete a customer: ", e);
		}
		return result;
	}

}
