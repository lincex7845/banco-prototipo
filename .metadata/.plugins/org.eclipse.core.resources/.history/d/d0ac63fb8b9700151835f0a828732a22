/**
 * 
 */
package com.mera.bank.api.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.mera.bank.api.persistence.dao.AccountDAO;
import com.mera.bank.api.persistence.dao.CustomerDAO;
import com.mera.bank.api.persistence.dao.MovementDAO;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.api.persistence.util.QueryOperator;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;
import com.mera.bank.common.entities.Movement;

/**
 * @author david.mera 
 * Implement BankService in order to save data into the
 *         database
 */
@Service
@Component("bankPersistenceService")
//@Qualifier(value = "bankPersistenceService")
public class BankPersistenceServiceImpl implements BankService {

	private CustomerDAO customersRepository;
	private AccountDAO accountsRepository;
	private MovementDAO movementsRepository;

	/**
	 * 
	 * @see com.mera.bank.api.services.BankService#createBankCustomer(com.mera.bank.
	 *      common.entities.Customer)
	 *
	 * @throws IllegalStateException
	 *             If a constrain validation violation is raised
	 * 
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * 
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * 
	 * @throws IllegalArgumentException
	 *             It the customer to be inserted is null
	 */
	@Override
	public void createBankCustomer(Customer newCustomer) {
		customersRepository.addCustomer(newCustomer);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getBankCustomerByIdAndName(java.
	 *      lang.String, java.lang.String)
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up, or, indicates an
	 *             invalid configuration or invalid mapping information
	 */
	@Override
	public Customer getBankCustomerByIdAndName(String customerDocumentId, String customerName) {
		QueryFilterUtil[] filters = new QueryFilterUtil[] {
				new QueryFilterUtil("customerName", QueryOperator.EQUALS, customerName),
				new QueryFilterUtil("customerDocumentId", QueryOperator.EQUALS, customerDocumentId) };
		List<Customer> customers = customersRepository.getCustomersByCriteria(filters);
		if (customers.isEmpty())
			return null;
		return customers.get(0);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getAllBankCustomers()
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up, or, indicates an
	 *             invalid configuration or invalid mapping information
	 */
	@Override
	public List<Customer> getAllBankCustomers() {
		return customersRepository.getAllCustomers();
	}

	/**
	 * @see com.mera.bank.api.services.BankService#getBankCustomerById(java.lang.
	 *      String)
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 */
	@Override
	public Customer getBankCustomerById(String customerDocumentId) {
		return customersRepository.getCustomerById(customerDocumentId);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#updateBankCustomer(java.lang.
	 *      String, com.mera.bank.common.entities.Customer)
	 * @throws IllegalStateException
	 *             If a constrain validation violation is raised
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             It the new data to be inserted is null
	 */
	@Override
	public void updateBankCustomer(String customerDocumentId, Customer updatedCustomer) {
		customersRepository.updateCustomer(customerDocumentId, updatedCustomer);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#deleteBankCustomer(java.lang.
	 *      String)
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws UnsupportedOperationException
	 *             When a null record is passed as parameter
	 */
	@Override
	public void deleteBankCustomer(String customerDocumentId) {
		customersRepository.deleteCustomer(customerDocumentId);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#createBankAccount(com.mera.bank.
	 *      common.entities.Account)
	 * @throws IllegalStateException
	 *             If a validation constraint violation happens
	 * @throws PersistenceException
	 *             Indicates: <br>
	 *             <list>
	 *             <ul>
	 *             An invalid configuration or invalid mapping information
	 *             </ul>
	 *             <ul>
	 *             A problem opening the session, checking the transaction
	 *             status,committing the transaction or cleaning up
	 *             </ul>
	 *             <ul>
	 *             When a null record is passed as parameter
	 *             </ul>
	 *             </list>
	 */
	@Override
	public void createBankAccount(Account newAccount) {
		accountsRepository.addAccount(newAccount);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getBankAccountsByCustomerId(java.
	 *      lang.String)
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	@Override
	public List<Account> getBankAccountsByCustomerId(String customerId) {
		QueryFilterUtil filter = new QueryFilterUtil("customerDocumentId", QueryOperator.EQUALS, customerId);
		return accountsRepository.getAccountsByCriteria(filter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mera.bank.api.services.BankService#getAccountById(java.lang.String)
	 */
	@Override
	public Account getAccountById(String accountId) {
		return accountsRepository.getAccountById(accountId);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getAccountByNumber(java.lang.
	 *      String)
	 * 
	 * @throws PersistenceException
	 *             - Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	@Override
	public Account getAccountByNumber(String accountNumber) {
		QueryFilterUtil filter = new QueryFilterUtil("accountNumber", QueryOperator.EQUALS, accountNumber);
		List<Account> accounts = accountsRepository.getAccountsByCriteria(filter);
		if (accounts.isEmpty())
			return null;
		else
			return accounts.get(0);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.bank.api.services.BankService#deleteBankAccount(java.lang.
	 * String)
	 */
	@Override
	public void deleteBankAccount(String accountId) {
		accountsRepository.deleteAccount(accountId);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#createBankMovement(com.mera.bank.
	 *      common.entities.Movement)
	 * 
	 * @throws PersistenceException
	 *             - Usually indicates an invalid configuration or invalid
	 *             mapping information
	 * 
	 * @throws HibernateException
	 *             - Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * 
	 * @throws IllegalArgumentException
	 *             - When a null record is passed as parameter
	 */
	@Override
	public void createBankMovement(Movement newMovement) {
		movementsRepository.addMovement(newMovement);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#
	 *      getMovementByDateRangeAndAccountNumber(java.lang.String,
	 *      java.util.Date, java.util.Date)
	 * 
	 * @throws PersistenceException
	 *             - Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 * @throws NullPointerException
	 *             - if some date object is null.
	 */
	@Override
	public List<Movement> getMovementByDateRangeAndAccountNumber(String accountNumber, final Date initialDate,
			final Date finalDate) {
		QueryFilterUtil filter = new QueryFilterUtil("accountNumber", QueryOperator.EQUALS, accountNumber);
		List<Movement> movements = movementsRepository.getMovementsBy(filter);
		Predicate<Movement> predicateForDate = new Predicate<Movement>() {
			@Override
			public boolean apply(Movement movement) {
				Date testedDate = movement.getMovementDate();
				return (testedDate.after(initialDate) || testedDate.equals(initialDate))
						&& (testedDate.before(finalDate) || testedDate.equals(finalDate));
			}
		};
		java.util.Collection<Movement> movementsInRange = Collections2.filter(movements, predicateForDate);
		return new ArrayList<Movement>(movementsInRange);
	}

}
