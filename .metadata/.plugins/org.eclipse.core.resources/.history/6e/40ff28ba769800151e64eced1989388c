/**
 * 
 */
package com.mera.bank.webApp.controllers;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.mera.bank.webApp.models.AccountModel;
import com.mera.bank.webApp.models.CustomerModel;
import com.mera.bank.webApp.services.AccountService;
import com.mera.bank.webApp.services.CustomerService;
import com.sun.jersey.api.client.ClientResponse;

/**
 * @author david.mera
 *
 */
public class AccountController extends SelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8481262019591827751L;

	private static final Logger LOGGER = Logger.getLogger(AccountController.class.getName());

	@Wire
	private Textbox accountNumber;

	@Wire
	private Listbox customerList;

	@Wire
	private Listbox accountList;

	@Wire
	private Textbox accountNumberFilter;

	@Wire
	private Listbox customerListFilter;

	@Wire
	private Button addAccountBtn;

	@Override
	public void doAfterCompose(Component component) throws Exception {
		super.doAfterCompose(component); // wire variables and event listeners
		if ("parentLayout".equals(component.getId())) {
			ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(
					CustomerService.getBankCustomers());
			customerModel.addToSelection(customerModel.get(0));
			customerList.setModel(customerModel);
			customerListFilter.setModel(customerModel);
			if (!customerModel.isEmpty()) {
				addAccountBtn.setDisabled(false);
				customerList.setSelectedItem(customerList.getItemAtIndex(0));
				customerListFilter.setSelectedItem(customerListFilter.getItemAtIndex(0));
			}
		}
	}
	
	@Listen("onClick = #addAccountBtn")
	public void addAccount(){
		String customerDocumentId = customerList.getSelectedItem().getValue();
		AccountModel account = new AccountModel(null, accountNumber.getValue(), BigDecimal.ZERO, customerDocumentId, false);
		ClientResponse response = AccountService.addBankAccount(account);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_CREATED) {
				Messagebox.show(entity, "Add Account", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to add account. HTTP %d - %s", status, entity));
				Messagebox.show(entity, "Failed Add Account", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			Messagebox.show("It is not posible connect to server", "Failed Add Account", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
	
	@Command
	public void filterAccounts(){
		String customerDocumentId = customerListFilter.getSelectedItem().getValue();
		String number = accountNumberFilter.getValue();
		AccountModel[] accounts =  AccountService.getAccountsByNumberAndCustomerId(number, customerDocumentId);
		ListModelList<AccountModel> accountModel = new ListModelList<AccountModel>(accounts);
		accountList.setModel(accountModel);
	}

}
