/**
 * 
 */
package com.mera.bank.api.persistence.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.common.entities.Customer;

/**
 * Data Access Object created to manipulate data on Customers table
 * 
 * @author david.mera
 *
 */
@Repository
public class CustomerDAO extends AbstractDAO<Customer> {

	/***
	 * The constructor by default
	 */
	public CustomerDAO() {
		super(Customer.class);
	}

	/***
	 * Gets the hibernate criteria for Customer entities *
	 * 
	 * @return The hibernate criteria for Customer entites
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session
	 * 
	 */
	public Criteria getCustomerCriteria() {
		return this.getEntityCriteria();
	}

	/***
	 * Gets all records stored on Customers table
	 * 
	 * @return List of all customers
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	public List<Customer> getAllCustomers() {
		return this.getAllRecords();
	}

	/***
	 * This method allows to get customer by several criteria
	 * 
	 * @param queryFilters
	 *            (Optional) zero or many criteria to filter customers
	 * @return List of customers which satisfies criteria
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	public List<Customer> getCustomersByCriteria(
			QueryFilterUtil... queryFilters) {
		return this.getRecordsByCriteria(queryFilters);
	}

	/***
	 * This method allows to get customer by a given id
	 * 
	 * @param id
	 *            The customer's identifier
	 * @return The customer identified by the given id
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 */
	public Customer getCustomerById(String id) {
		return this.getRecordById(id);
	}

	/***
	 * This method allows to insert data into Customers table
	 * 
	 * @param newCustomer
	 *            The new customer to insert
	 * @throws IllegalStateException
	 *             If a constrain validation violation is raised
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             It the customer to be inserted is null
	 */
	public void addCustomer(Customer newCustomer) {
		boolean integrityFailed = false;
		try {
			this.saveRecord(newCustomer);
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			integrityFailed = true;
			throw new IllegalStateException(e.getMessage(), e);
		} finally {
			if (integrityFailed)
				this.closeSession();
		}
	}

	/***
	 * This method allows to update the customer's name by a given customer's Id
	 * 
	 * @param id
	 *            The id which identifies the customer to be updated
	 * @param updatedCustomer
	 *            Contains the new data to set on the customer selected
	 * @throws IllegalStateException
	 *             If a constrain validation violation is raised
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             It the new data to be inserted is null
	 */
	public void updateCustomer(String id, Customer updatedCustomer) {
		Customer oldCustomer = getCustomerById(id);
		if (oldCustomer != null) {
			boolean integrityFailed = false;
			try {
				oldCustomer.setCustomerName(updatedCustomer.getCustomerName());
				this.updateRecord(oldCustomer);
			} catch (org.hibernate.exception.ConstraintViolationException e) {
				integrityFailed = true;
				throw new IllegalStateException(e.getMessage(), e);
			} finally {
				if (integrityFailed)
					this.closeSession();
			}
		}
	}

	/***
	 * This method allows to delete a customer by a given Id
	 * 
	 * @param id
	 *            The id which identifies the customer
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws UnsupportedOperationException
	 *             When a null record is passed as parameter
	 */
	public void deleteCustomer(String id) {
		Customer customer = getCustomerById(id);
		if (customer != null) {
			this.deleteRecord(customer);
		}
	}

}