/**
 * 
 */
package com.mera.bank.webApp.controllers;

import java.math.BigDecimal;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import com.mera.bank.webApp.models.MovementModel;
import com.mera.bank.webApp.models.MovementType;
import com.mera.bank.webApp.services.MovementService;
import com.sun.jersey.api.client.ClientResponse;

/**
 * @author david.mera
 *
 */
public class MovementController extends SelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5017522068637032386L;
	
	private static final Logger LOGGER = Logger.getLogger(MovementController.class.getName());

	@Wire
	private Window newMovementDialog;
	@Wire
	private Label accountNumber;
	@Wire
	private Combobox movementType;
	@Wire
	private Doublebox movementValue;
	@Wire
	private Button addMovementBtn;
	@Wire
	private Button cancelMovementBtn;
	@Wire
	private Button filterMovementsBtn;
	@Wire
	private Textbox accountNumberFilter;
	@Wire
	private Datebox initialDateFilter;
	@Wire
	private Datebox finalDateFilter;
	@Wire
	private Label accountNumberFiltered;
	@Wire
	private Label accountCustomerIdFiltered;
	@Wire
	private Label accountBalanceFiltered;
	
	
	@Listen("onCreate = #newMovementDialog")
	public void showDialog(CreateEvent event) {
		accountNumber.setValue((String) event.getArg().get("accountNumber"));
		movementType.setSelectedIndex(0);
	}
	
	@Listen("onClick = #addMovementBtn")
	public void addMovement(){
		String number = accountNumber.getValue();
		String typeMovement = movementType.getValue();
		BigDecimal value = new BigDecimal(movementValue.getValue());
		MovementModel newMovement = new MovementModel();
		newMovement.setAccountNumber(number);
		newMovement.setMovementType(MovementType.valueOf(typeMovement));
		newMovement.setMovementValue(value);
		ClientResponse response = MovementService.addMovement(newMovement);
		if(response != null){
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_CREATED) {
				Messagebox.show(entity, "Add Movement", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to add movement. HTTP %d - %s", status, entity));
				Messagebox.show(entity, "Failed Add Customer", Messagebox.OK, Messagebox.ERROR);
			}
		}
		else{
			Messagebox.show("It is not posible connect to server", "Failed Add Customer", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
	
	@Listen("onClick = #cancelMovementBtn")
	public void cancelMovement(){
		newMovementDialog.detach();
	}
	
	@Listen("onClick = #filterMovementsBtn")~
	public void getMovements(){
		
	}
}
