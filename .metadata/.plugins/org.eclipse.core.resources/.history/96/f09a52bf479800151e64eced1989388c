/**
 * 
 */
package com.mera.bank.webApp.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.mera.bank.webApp.models.CustomerModel;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.jboss.logging.Logger;

/**
 * @author david.mera
 *
 */
public class BankService {

	private static Gson gson;
	private static Client client;
	private static final String serviceHost;
	private static final Logger logger;

	static {
		gson = new Gson();
		client = Client.create();
		serviceHost = "http://localhost:9000";
		logger = Logger.getLogger(BankService.class.getName());
	}

	public static boolean addBankCustomer(CustomerModel customerModel) {
		// ** Request
		boolean result = false;
		AsyncWebResource awr = client.asyncResource(serviceHost + "/customers/add");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				gson.toJson(customerModel));
		logger.info("Customer to add: "
				+ String.format("%s - %s", customerModel.getCustomerDocumentId(), customerModel.getCustomerName()));
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			switch (status) {
			case HttpServletResponse.SC_CREATED:
				result = true;
				break;
			default:
				logger.error(String.format("Failed attempt to add customer. HTTP %d - %s", status, entity));
				break;
			}
		} catch (InterruptedException | ExecutionException e) {
			logger.error("Raised exception when trying to add customer: ", e);
		}
		return result;
	}

	public static CustomerModel[] getBankCustomers() {
		CustomerModel[] customers = new CustomerModel[0];
		AsyncWebResource awr = client.asyncResource(serviceHost + "/customers");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		logger.info("Request for getting customers");
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			switch (status) {
			case HttpServletResponse.SC_OK:
				customers = gson.fromJson(entity, CustomerModel[].class);
				break;
			default:
				logger.error(String.format("Failed attempt to get customers. HTTP %d - %s", status, entity));
				break;
			}
		} catch (InterruptedException | ExecutionException e) {
			logger.error("Raised exception when trying to get customers: ", e);
		}
		return customers;
	}

	public static boolean updateBankCustomer(String customerDocumentId, String customerNewName) {
		boolean result = false;
		AsyncWebResource awr = client.asyncResource(serviceHost + "/customers/update/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class,
				customerNewName);
		logger.info("Request for updating customer " + customerDocumentId);
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			switch (status) {
			case HttpServletResponse.SC_OK:
				result = true;
				break;
			default:
				logger.error(String.format("Failed attempt to update customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
				break;
			}
		} catch (InterruptedException | ExecutionException e) {
			logger.error("Raised exception when trying to update a customer: ", e);
		}
		return result;
	}

	public static boolean deleteBankCustomer(String customerDocumentId) {
		boolean result = false;
		AsyncWebResource awr = client.asyncResource(serviceHost + "/customers/delete/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
		logger.info("Request for deleting customer " + customerDocumentId);
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			switch (status) {
			case HttpServletResponse.SC_OK:
				result = true;
				break;
			default:
				logger.error(String.format("Failed attempt to delete customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
				break;
			}
		} catch (InterruptedException | ExecutionException e) {
			logger.error("Raised exception when trying to delete a customer: ", e);
		}
		return result;
	}

}
