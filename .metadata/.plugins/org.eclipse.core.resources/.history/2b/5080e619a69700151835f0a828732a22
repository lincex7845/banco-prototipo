/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.utilities.StringValidator;
import com.mera.bank.common.entities.Customer;
import com.mera.bank.common.entities.Movement;

/**
 * @author david.mera This resources process all HTTP requests related to
 *         movements (bank transactions)
 */
@Path("/movements")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MovementResource {

	private final BankService bankSpaceService;
	private final Gson gson;

	/**
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to accounts
	 */
	public MovementResource(BankService bankService) {
		bankSpaceService = bankService;
		gson = new GsonBuilder().create();
	}

	@POST
	@Path("/add")
	public Response addBankMovement(Movement movement) {
		Response response = null;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankMovement(movement);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The movement was created";
		} catch (org.openspaces.core.EntryAlreadyInSpaceException e) {
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The movement already exists into the database";
		} catch (IllegalArgumentException e) {
			status = Response.Status.BAD_REQUEST.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@GET
	public Response getMovementByDateRangeAndAccountNumber(@Context UriInfo uriInfo){
		Response response = null;
		int status = 0;
		String entity = null;
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		try {
			if (queryParams.size() > 0) {
				Long initialDate = null;
				Long finalDate = null;
				String accountNumber = null;
				for (Entry<String, List<String>> entry : queryParams.entrySet()) {
					String key = entry.getKey();
					switch (key){
					case "initialDate":
						initialDate = Long.parseLong(entry.getValue().get(0));
						break;
					case "finalDate":
						finalDate = Long.parseLong(entry.getValue().get(0));
						break;
					case "accountNumber":
						accountNumber = entry.getValue().get(0);
						break;
					}
				}
				if (StringValidator.isValidString(accountNumber)) {					
					List<Movement> movements = bankSpaceService.getMovementByDateRangeAndAccountNumber(accountNumber, 
							new DateTime(initialDate).toDate(), new DateTime(finalDate).toDate());
					Collections.sort(movements, new Comparator<Movement>() {
						public int compare(Movement a, Movement b) {
							return a.getMovementDate().compareTo(b.getMovementDate()) + a.getId().compareTo(b.getId());
						}
					});
					status = Response.Status.OK.getStatusCode();
					entity = gson.toJson(movements);
				} else {
					status = Response.Status.BAD_REQUEST.getStatusCode();
					entity = "The parameters sent are not allowed.";
				}
			}
			else{
				status = Response.Status.BAD_REQUEST.getStatusCode();
				entity = "The range of dates and account's number are required in order to fetch the movements"; 
			}
		}
		response = Response.status(status).entity(entity).build();
		return response;
		
	}
}
