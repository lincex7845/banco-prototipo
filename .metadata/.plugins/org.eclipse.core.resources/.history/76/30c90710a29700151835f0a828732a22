/**
 * 
 */
package com.mera.bank.api.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;

/**
 * @author david.mera This resources process all HTTP requests related to
 *         movements (bank transactions)
 */
@Path("/movements")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MovementResource {

	private final BankService bankSpaceService;
	private final Gson gson;

	/**
	 * Constructor
	 * 
	 * @param bankService
	 *            Service which contains business logic to process requests
	 *            related to accounts
	 */
	public MovementResource(BankService bankService) {
		this.bankSpaceService = bankService;
		gson = new GsonBuilder().create();
	}
}
