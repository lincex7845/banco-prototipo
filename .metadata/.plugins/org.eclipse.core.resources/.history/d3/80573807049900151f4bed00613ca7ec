package com.mera.bank.processor;

import java.math.BigDecimal;
import org.jboss.logging.Logger;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gigaspaces.client.WriteModifiers;
import com.google.gson.Gson;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Movement;
import com.mera.bank.common.enumerations.MovementType;

/**
 * This object process bank transactions (movements) in order to update the
 * balance in bank accounts
 * 
 * @author david.mera
 *
 */
public class TransactionProcessor {

	/**
	 * Injected dependency to bank's space
	 */
	@Autowired
	private GigaSpace gigaSpace;
	/***
	 * The logger object
	 */
	private static final Logger LOGGER = Logger.getLogger(TransactionProcessor.class
			.getName());

	/***
	 * The constructor by default
	 */
	public TransactionProcessor() {
		//Used by spring to initialize PU
	}

	/**
	 * This method process a bank transaction (movement) by updating the balance
	 * into the bank account requested by the transaction itself. Negative
	 * balance are not be settled by this method
	 * 
	 * @param newMovement
	 *            The movement to be processed
	 * @return The processed movement
	 */
	@Transactional(readOnly = false,
            propagation = Propagation.REQUIRES_NEW,
            isolation  = Isolation.READ_COMMITTED)
	public Movement processTransaction(Movement newMovement) {
		LOGGER.info("Incoming transaction: " + new Gson().toJson(newMovement));
		if (newMovement == null) {
			LOGGER.error("Null movements are not allowed");
		} else {
			Account accountTemplate = new Account();
			accountTemplate.setAccountNumber(newMovement.getAccountNumber());
			if (!newMovement.getIsProcessed()) {
				Account bankAccount = (Account) gigaSpace.read(accountTemplate,
						500);
				if (bankAccount == null) {
					LOGGER.warn("The bank account "
							+ newMovement.getAccountNumber()
							+ " is not registred yet");
				} else {
					BigDecimal money = bankAccount.getAccountBalance() == null ? new BigDecimal(
							0) : bankAccount.getAccountBalance();
					BigDecimal balance = newMovement.getMovementType() == MovementType.DEBIT ? money
							.add(newMovement.getMovementValue()) : money
							.subtract(newMovement.getMovementValue());
					// This line check if the new balance will be less than
					// zero.
					// Negative balance is not allowed
					if (balance.compareTo(BigDecimal.ZERO) == -1) {
						LOGGER.error("Negative balance is not allowed in bank accounts.");
					} else {
						// The new value for the balance is settled on the account
						bankAccount.setAccountBalance(balance);
					}
					// The bank account is updated on the space
					gigaSpace.write(bankAccount, WriteModifiers.PARTIAL_UPDATE);
				}
				newMovement.setIsProcessed(Boolean.TRUE);
			}
			LOGGER.info("PROCESSED Transaction: " + newMovement.getId());
		}
		return newMovement;
	}
}
