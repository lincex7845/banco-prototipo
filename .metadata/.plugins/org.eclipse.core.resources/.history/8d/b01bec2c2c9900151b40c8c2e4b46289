<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
	xmlns:os-core="http://www.openspaces.org/schema/core" xmlns:tx="http://www.springframework.org/schema/tx"
	xmlns:os-events="http://www.openspaces.org/schema/events"
	xmlns:os-remoting="http://www.openspaces.org/schema/remoting"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
       http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.2.xsd
       http://www.openspaces.org/schema/core http://www.openspaces.org/schema/10.0/core/openspaces-core.xsd
       http://www.openspaces.org/schema/events http://www.openspaces.org/schema/10.0/events/openspaces-events.xsd
       http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-3.2.xsd
       http://www.openspaces.org/schema/remoting http://www.openspaces.org/schema/10.0/remoting/openspaces-remoting.xsd">


	<!-- Spring property configurer which allows us to use system properties 
		(such as user.name). -->
	<bean id="propertiesConfigurer"
		class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />

	<!-- A JDBC pooled data source that connects to the Postgres server. -->
	<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"
		destroy-method="close">
		<property name="driverClassName" value="org.postgresql.Driver" />
		<property name="url" value="jdbc:postgresql://localhost:5432/banco_db" />
		<property name="username" value="banco_user" />
		<property name="password" value="banco_Password#9" />
	</bean>

	<!-- Hibernate SessionFactory bean. Uses the pooled data source to connect 
		to the database. -->
	<bean id="sessionFactory"
		class="org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean">
		<property name="dataSource" ref="dataSource" />
		<property name="annotatedClasses">
			<list>
				<value>com.mera.bank.common.entities.Customer</value>
				<value>com.mera.bank.common.entities.Account</value>
				<value>com.mera.bank.common.entities.Movement</value>
			</list>
		</property>
		<property name="hibernateProperties">
			<props>
				<!-- SQL dialect -->
				<prop key="hibernate.dialect">org.hibernate.dialect.PostgreSQLDialect</prop>
				<!-- Print executed SQL to stdout -->
				<prop key="show_sql">true</prop>
				<prop key="hibernate.hbm2ddl.auto">update</prop>
				<prop key="hibernate.cache.provider_class">org.hibernate.cache.NoCacheProvider</prop>
				<prop key="hibernate.cache.use_second_level_cache">false</prop>
				<prop key="hibernate.cache.use_query_cache">false</prop>
				<prop key="hibernate.jdbc.batch_size">50</prop>
			</props>
		</property>
	</bean>

	<bean id="hibernateSpaceDataSource"
		class="org.openspaces.persistency.hibernate.DefaultHibernateSpaceDataSourceFactoryBean">
		<property name="sessionFactory" ref="sessionFactory" />
		<property name="initialLoadChunkSize" value="2000" />
		<property name="performOrderById" value="true" />
	</bean>

	<bean id="hibernateSpaceSynchronizationEndpoint"
		class="org.openspaces.persistency.hibernate.DefaultHibernateSpaceSynchronizationEndpointFactoryBean">
		<property name="sessionFactory" ref="sessionFactory" />
	</bean>

	<!-- A bean representing a space (an IJSpace implementation). Note, we do 
		not specify here the cluster topology of the space. It is declated outside 
		of the processing unit or within the SLA bean. The space is configured to 
		connect to a mirror, and uses the configured external data source to perform 
		the initial load operation from the database when the Space starts up. -->
	<os-core:space id="space" url="/./bankSpace" schema="persistent"
		space-data-source="hibernateSpaceDataSource" space-sync-endpoint="hibernateSpaceSynchronizationEndpoint"
		lookup-groups="gigaspaces-10.0.1-XAPPremium-ga">
		<os-core:properties>
			<props>
				<!-- Use ALL IN CACHE - Read Only from the database -->
				<!-- <prop key="space-config.engine.cache_policy">1</prop> -->
				<prop key="cluster-config.cache-loader.external-data-source">true</prop>
				<prop key="cluster-config.cache-loader.central-data-source">true</prop>
				<!-- <prop key="cluster-config.mirror-service.url"> -->
				<!-- jini://*/mirror-service_container/mirror-service -->
				<!-- </prop> -->
				<!-- bank-mirror-service_container -->
				<!-- <prop key="cluster-config.mirror-service.bulk-size">100</prop> -->
				<!-- <prop key="cluster-config.mirror-service.interval-millis">2000</prop> -->
				<!-- <prop key="cluster-config.mirror-service.interval-opers">100</prop> -->
				<!-- <prop key="cluster-config.groups.group.repl-policy.repl-oriCginal-state">true</prop> -->
			</props>
		</os-core:properties>
	</os-core:space>

	<!-- Defines a distributed transaction manager. -->
	<os-core:distributed-tx-manager id="transactionManager"
		default-timeout="120" />

	<tx:annotation-driven transaction-manager="transactionManager" />

	<!-- OpenSpaces simplified space API built on top of IJSpace/JavaSpace. tx-manager="transactionManager" -->
	<os-core:giga-space id="gigaSpace" space="space"
		/>

	<bean id="customerProcessor" class="com.mera.bank.processor.CustomerProcessor" />
	<bean id="accountProcessor" class="com.mera.bank.processor.AccountProcessor"></bean>
	<bean id="transactionProcessor" class="com.mera.bank.processor.TransactionProcessor" />

	<!-- A polling event container that perfoms (by default) polling take operations 
		against the space using the provided template (in our case, and the non processed 
		data objects). Once a match is found, the data processor bean event listener 
		is triggered using the method adapter. <os-events:tx-support tx-manager="transactionManager" /> -->

	<os-events:polling-container id="customerProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">		
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Customer">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="saveCustomer">
				<os-events:delegate ref="customerProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

	<os-events:polling-container id="accountProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Account">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="saveAccount">
				<os-events:delegate ref="accountProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

	<os-events:polling-container id="TransactionProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Movement">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="processTransaction">
				<os-events:delegate ref="transactionProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

</beans>