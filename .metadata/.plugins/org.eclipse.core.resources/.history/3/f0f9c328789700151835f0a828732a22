/**
 * 
 */
package com.mera.bank.api.persistence.dao;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.common.entities.Movement;

/**
 * The Data Access Object to manipulate data from Movements Table
 * 
 * @author david.mera
 *
 */
@Repository
public class MovementDAO extends AbstractDAO<Movement> {

	/***
	 * The constructor by default
	 */
	public MovementDAO() {
		super(Movement.class);
	}

	/***
	 * This method allows to insert data on the movements table
	 * 
	 * @param newMovement
	 *            The new movement to be inserted
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session or Indicates a
	 *             problem when checking the transaction status, committing the
	 *             transaction or cleaning up
	 * @throws IllegalArgumentException
	 *             When a null record is passed as parameter
	 */
	public void addMovement(Movement newMovement) {
		this.saveRecord(newMovement);
	}

	/***
	 * This method gets the movements by several criteria.
	 * 
	 * @param queryFilters
	 *            (Optional) Zero or many criteria which filter the movements
	 * @return List of movements which satisfies the given criteria
	 * @throws PersistenceException
	 *             Indicates a problem when checking the transaction status,
	 *             committing the transaction or cleaning up Or indicates an
	 *             invalid configuration or invalid mapping information
	 */
	public List<Movement> getMovementsBy(QueryFilterUtil... queryFilters) {
		return this.getRecordsByCriteria(queryFilters);
	}

}