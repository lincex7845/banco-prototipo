/**
 * 
 */
package com.mera.bank.api.services;

import java.util.List;
import com.mera.bank.api.persistence.dao.AccountDAO;
import com.mera.bank.api.persistence.dao.CustomerDAO;
import com.mera.bank.api.persistence.util.QueryFilterUtil;
import com.mera.bank.api.persistence.util.QueryOperator;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;

/**
 * @author david.mera
 *
 */
public class BankBL {

	private static CustomerDAO customersRepository;
	private static AccountDAO accountsRepository;

	static {
		customersRepository = new CustomerDAO();
		accountsRepository = new AccountDAO();
	}

	public static void createPersistedBankCustomer(Customer newCustomer) {
		customersRepository.addCustomer(newCustomer);
	}

	public static List<Customer> getBankCustomersBy(QueryFilterUtil... filters) {
		return customersRepository.getCustomersByCriteria(filters);
	}

	public static void updatePersistedBankCustomer(Customer updatedCustomer, String customerId) {
		if (updatedCustomer==null)
			throw new IllegalArgumentException("Null customers are not allowed.");
		customersRepository.updateCustomer(customerId, updatedCustomer);
	}

	public static void deletePersistedBankCustomer(String customerId) {
		customersRepository.deleteCustomer(customerId);
	}

	public static void deletePersistedBankAccount(String customerId, String accountNumber){
		accountsRepository.deleteAccount(customerId, accountNumber);
	}
	
	public static Account getPersistedBankAccountById(String accountId){
		return accountsRepository.getAccountById(accountId);
	}
	
	public static Account getPersistedBankAccountByNumber(String accountNumber){
		return accountsRepository.getAccountsByCriteria(new QueryFilterUtil("accountNumber", QueryOperator.EQUALS, accountNumber)).get(0);
	}
}
