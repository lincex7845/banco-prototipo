/**
 * 
 */
package com.mera.bank.api;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mera.bank.api.services.BankService;
import com.mera.bank.api.services.SpaceBankBL;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;
import com.mera.bank.common.entities.Movement;
import com.mera.bank.common.enumerations.MovementType;

/**
 * @author david.mera
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:com/mera/bank/api/SpaceBankBLIntegrationContextTest-context.xml")
public class SpaceBankBLIntegrationTest {

	@Autowired
	protected GigaSpace gigaSpace;
	private final static int MAX_CUSTOMERS = 30;
	// protected SpaceBankBL bankBL = new SpaceBankBL();
	@Resource
	BankService bankSpaceService;

	protected String[] getConfigLocations() {
		return new String[] { "classpath:com/mera/bank/api/SpaceBankBLIntegrationContextTest-context.xml" };
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUpBefore() throws Exception {
		gigaSpace.clear(null);
		// bankBL.setGigaSpace(gigaSpace);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDownAfter() throws Exception {
		gigaSpace.clear(null);
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#createBankCustomer(com.mera.bank.common.entities.Customer)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testCreateBankCustomer() throws InterruptedException {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		bankSpaceService.createBankCustomer(cliente);
		Thread.sleep(100);
		// ** Verify
		Customer customerProcessed = bankSpaceService.getBankCustomerByIdAndName(cliente.getCustomerDocumentId(),
				cliente.getCustomerName());
		assertNotNull(customerProcessed);
		assertTrue(customerProcessed.getIsProcessed());
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#getAllBankCustomers()} .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testGetAllBankCustomers() throws InterruptedException {
		for (int i = 1; i <= MAX_CUSTOMERS; i++) {
			Customer cliente = new Customer("Julio_" + i, "12969860" + i, Boolean.FALSE);
			bankSpaceService.createBankCustomer(cliente);
		}
		Thread.sleep(100);
		// ** Verify
		List<Customer> customers = bankSpaceService.getAllBankCustomers();
		assertNotNull(customers);
		assertTrue(!customers.isEmpty());
		assertTrue(customers.size() >= 30);
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#getBankCustomersBy(java.lang.String, java.lang.String)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testGetBankCustomersBy() throws InterruptedException {
		for (int i = 1; i <= MAX_CUSTOMERS; i++) {
			Customer cliente = new Customer("Name_" + i, "1" + i, Boolean.FALSE);
			bankSpaceService.createBankCustomer(cliente);
		}
		Thread.sleep(100);
		// ** Verify
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		String documentId = "1" + id;
		String name = "Name_" + id;
		Customer customerProcessed = bankSpaceService.getBankCustomerByIdAndName(documentId, name);
		assertNotNull(customerProcessed);
		assertTrue(customerProcessed.getIsProcessed());
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#updateBankCustomer(java.lang.String, java.lang.String)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testUpdateBankCustomer() throws InterruptedException {
		for (int i = 1; i <= MAX_CUSTOMERS; i++) {
			Customer cliente = new Customer("Name_" + i, "1" + i, Boolean.FALSE);
			bankSpaceService.createBankCustomer(cliente);
		}
		Thread.sleep(100);
		// ** Verify
		Random r = new Random();
		int id = r.nextInt((MAX_CUSTOMERS - 1) + 1) + 1;
		String documentId = "1" + id;
		String name = "Name_" + id;
		String newName = name + "Jorge";
		Customer customerProcessed1 = bankSpaceService.getBankCustomerByIdAndName(documentId, name);
		bankSpaceService.updateBankCustomer(documentId, new Customer(newName, documentId, null));
		// ** Verify
		Customer customerProcessed3 = bankSpaceService.getBankCustomerByIdAndName(documentId, newName);
		assertNotNull(customerProcessed1);
		assertNotNull(customerProcessed3);
		assertTrue(customerProcessed1.getIsProcessed());
		assertTrue(customerProcessed3.getIsProcessed());
		assertEquals(customerProcessed3.getCustomerName(), newName);
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#deleteBankCustomer(java.lang.String)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test(expected = NoSuchElementException.class)
	public void testDeleteBankCustomer() throws InterruptedException {
		for (int i = 1; i <= MAX_CUSTOMERS; i++) {
			Customer cliente = new Customer("Name_" + i, "1" + i, Boolean.FALSE);
			bankSpaceService.createBankCustomer(cliente);
		}
		Thread.sleep(100);
		// ** Verify
		Random r = new Random();
		int id = r.nextInt(MAX_CUSTOMERS);
		String documentId = "1" + id;
		String name = "Name_" + id;
		bankSpaceService.deleteBankCustomer(documentId);
		Customer deletedCustomer = bankSpaceService.getBankCustomerByIdAndName(documentId, name);
		assertNull(deletedCustomer);
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#addBankAccount(java.lang.String, com.mera.bank.common.entities.Account)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAddBankAccount() throws InterruptedException {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		bankSpaceService.createBankCustomer(cliente);
		Thread.sleep(50);
		Account bankAccount = new Account(null, "12*12", new BigDecimal(0), cliente.getCustomerDocumentId(),
				Boolean.FALSE);
		bankSpaceService.createBankAccount(bankAccount);
		Thread.sleep(50);
		// ** Verify
		Account ac = bankSpaceService.getAccountByNumber(bankAccount.getAccountNumber());
		assertNotNull(ac);
		assertTrue(ac.getAccountNumber().equals(bankAccount.getAccountNumber()) && ac.getIsProcessed());
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#getAccountByAccountNumber(java.lang.String)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testGetAccountByAccountNumber() throws InterruptedException {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		bankSpaceService.createBankCustomer(cliente);
		Thread.sleep(50);
		Account bankAccount = new Account(null, "12*12", new BigDecimal(0), cliente.getCustomerDocumentId(),
				Boolean.FALSE);
		bankSpaceService.createBankAccount(bankAccount);
		Thread.sleep(50);
		// ** Verify
		Account processed = bankSpaceService.getAccountByNumber(bankAccount.getAccountNumber());
		assertNotNull(processed);
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#deleteBankAccount(java.lang.String, java.lang.String)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test(expected = NoSuchElementException.class)
	public void testDeleteBankAccount() throws InterruptedException {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		bankSpaceService.createBankCustomer(cliente);
		Thread.sleep(100);
		Account bankAccount1 = new Account(null, "12*12", new BigDecimal(0), cliente.getCustomerDocumentId(),
				Boolean.FALSE);
		Account bankAccount2 = new Account(null, "12*13", new BigDecimal(0), cliente.getCustomerDocumentId(),
				Boolean.FALSE);
		bankSpaceService.createBankAccount(bankAccount1);
		bankSpaceService.createBankAccount(bankAccount2);
		Thread.sleep(100);
		Account processedAccount = bankSpaceService.getAccountByNumber(bankAccount2.getAccountNumber());
		bankSpaceService.deleteBankAccount(processedAccount.getId());
		Thread.sleep(50);
		// ** Verify
		assertNotNull(processedAccount);
		Account deletedAccount = bankSpaceService.getAccountByNumber(bankAccount2.getAccountNumber());
	}

	/**
	 * Test method for
	 * {@link com.mera.bank.api.services.SpaceBankBL#makeBankTransaction(com.mera.bank.common.entities.Transaction)}
	 * .
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testMakeBankTransaction() throws InterruptedException {
		Customer cliente = new Customer("Julio Mera", "307265256", Boolean.FALSE);
		bankBL.createBankCustomer(cliente);
		Thread.sleep(50);
		Account bankAccount1 = new Account(null, "12*12", new BigDecimal(0), cliente.getCustomerDocumentId(),
				Boolean.FALSE);
		bankBL.addBankAccount(bankAccount1);
		Thread.sleep(50);
		Movement newMovement1 = new Movement(null, new Date(), new BigDecimal(500), MovementType.DEBIT,
				bankAccount1.getAccountNumber(), Boolean.FALSE);
		Movement newMovement2 = new Movement(null, new Date(), new BigDecimal(100), MovementType.CREDIT,
				bankAccount1.getAccountNumber(), Boolean.FALSE);
		bankBL.makeBankTransaction(newMovement1);
		bankBL.makeBankTransaction(newMovement2);
		Thread.sleep(50);
		// ** Verify
		Account processed = bankBL.getAccountByAccountNumber(cliente.getCustomerDocumentId(),
				bankAccount1.getAccountNumber());
		assertNotNull(processed);
		assertTrue(processed.getIsProcessed());
		assertTrue(processed.getAccountBalance().equals(new BigDecimal(500 - 100)));
	}

	@Test
	public void getMovementsInRange() throws InterruptedException {
		String accountNumber = "12*12";
		Account bankAccount1 = new Account(null, accountNumber, new BigDecimal(0), "1", Boolean.FALSE);
		bankBL.addBankAccount(bankAccount1);
		Thread.sleep(50);
		DateTime today = new DateTime();
		final Date to_day = today.toDate();
		final Date tommorrow = today.plusDays(1).toDate();
		final Date yesterday = today.minusDays(2).toDate();
		Movement newMovement1 = new Movement(null, yesterday, new BigDecimal(500), MovementType.DEBIT, accountNumber,
				Boolean.FALSE);
		Movement newMovement2 = new Movement(null, to_day, new BigDecimal(100), MovementType.CREDIT, accountNumber,
				Boolean.FALSE);
		Movement newMovement3 = new Movement(null, tommorrow, new BigDecimal(100), MovementType.CREDIT, accountNumber,
				Boolean.FALSE);
		bankBL.makeBankTransaction(newMovement1);
		bankBL.makeBankTransaction(newMovement2);
		bankBL.makeBankTransaction(newMovement3);
		Thread.sleep(50);
		List<Movement> movements = bankBL.getMovementsInRange(accountNumber, yesterday, today.toDate());
		assertTrue(!movements.isEmpty() && movements.size() == 2);
		for (Movement m : movements) {
			assertTrue(m.getMovementDate().equals(yesterday) || m.getMovementDate().equals(to_day));
		}
	}

}
