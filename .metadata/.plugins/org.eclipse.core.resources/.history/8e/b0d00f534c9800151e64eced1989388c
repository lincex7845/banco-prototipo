/**
 * 
 */
package com.mera.bank.webApp.controllers;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.google.gson.Gson;
import com.mera.bank.webApp.models.CustomerModel;
import com.mera.bank.webApp.services.BankService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

/**
 * @author david.mera
 *
 */
public class CustomerController extends SelectorComposer<Component> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2366073496443917093L;

	private static final Logger LOGGER = Logger.getLogger(CustomerController.class.getName());

	@Wire
	private Textbox customerName;
	@Wire
	private Textbox customerId;
	@Wire
	private Listbox customerList;
	@Wire
	private Label customerNameLabel;
	@Wire
	private Label customerDocumentIdLabel;
	@Wire
	private Borderlayout parentLayout;
	@Wire
	private Button updateCustomerBtn;
	@Wire
	private Window updateCustomerDialog;
	@Wire
	private Label updatedCustomerId;
	@Wire
	private Textbox updatedCustomerName;

	@Listen("onTimer = #timer")
	public void onTimer() {
		customerList.setModel(new ListModelList<CustomerModel>(BankService.getBankCustomers()));
	}

	@Listen("onClick = #addCustomerBtn")
	public void addCustomer() {
		CustomerModel customer = new CustomerModel(customerId.getValue(), customerName.getValue(), false);
		ClientResponse response = BankService.addBankCustomer(customer);
		int status = response.getStatus();
		String entity = response.getEntity(String.class);
		ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(new CustomerModel[0]);
		if (status == HttpServletResponse.SC_CREATED) {
			customerModel = new ListModelList<CustomerModel>(BankService.getBankCustomers());
			Messagebox.show(entity, "Add Customer", Messagebox.OK, Messagebox.INFORMATION);
		} else {
			LOGGER.error(String.format("Failed attempt to add customer. HTTP %d - %s", status, entity));
			Messagebox.show(entity, "Failed Add Customer", Messagebox.OK, Messagebox.ERROR);
		}
		customerList.setModel(customerModel);
	}

	@Listen("onCreate = #customerList")
	public void loadCustomers() {
		ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(BankService.getBankCustomers());
		customerList.setModel(customerModel);
	}

	@Listen("onClick = #modifyCustomerBtn")
	public void updateCustomer(Event event) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		CustomerModel selectedCustomer = (CustomerModel) event.getData();
		map.put("documentId", selectedCustomer.getCustomerDocumentId());
		map.put("name", selectedCustomer.getCustomerName());
		Window window = (Window) Executions.createComponents("updateCustomerDialog.zul", null, map);
		window.doModal();
	}

	@Listen("onClick = #deleteCustomerBtn")
	public void deleteCustomer(final Event event) {
		Messagebox.show("Are you sure you want to delete?", "Alert !!", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener<Event>() {
					@Override
					public void onEvent(Event evt) throws InterruptedException {
						if ("onYes".equals(evt.getName())) {
							String customerDocumentId = (String) event.getData();
							ClientResponse response = BankService.deleteBankCustomer(customerDocumentId);
							int status = response.getStatus();
							String entity = response.getEntity(String.class);
							if (status == HttpServletResponse.SC_OK) {
								Messagebox.show(entity, "Delete Customer", Messagebox.OK, Messagebox.INFORMATION);
							} else {
								LOGGER.error(String.format("Failed attempt to delete customer %s. HTTP %d - %s",
										customerDocumentId, status, entity));
								Messagebox.show(entity, "Failed Delete Customer", Messagebox.OK, Messagebox.ERROR);
							}
						}
					}
				});
		ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(BankService.getBankCustomers());
		customerList.setModel(customerModel);
	}

	@Listen("onCreate = #updateCustomerDialog")
	public void showDialog(CreateEvent event) {
		updatedCustomerId.setValue((String) event.getArg().get("documentId"));
		updatedCustomerName.setValue((String) event.getArg().get("name"));
	}

	@Listen("onClick = #updateCustomerBtn")
	public void updateCustomer() {
		String newName = updatedCustomerName.getValue();
		String customerDocumentId = updatedCustomerId.getValue();
		boolean result = BankService.updateBankCustomer(customerDocumentId, newName);
		if (result) {
			updateCustomerDialog.detach();
		} else {

		}
	}

	@Listen("onClick = #cancelUpdateBtn")
	public void cancelUpdate() {
		updateCustomerDialog.detach();
	}
}