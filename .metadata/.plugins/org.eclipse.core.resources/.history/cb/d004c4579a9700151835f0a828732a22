/**
 * 
 */
package com.mera.bank.api.resources;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.openspaces.core.GigaSpace;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataAccessException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.bank.api.services.BankService;
import com.mera.bank.api.utilities.StringValidator;
import com.mera.bank.common.entities.Customer;

/**
 * @author david.mera
 *
 */
@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ComponentScan(basePackages = { "com.mera.bank.api.services" })
public class CustomerResource {
	private final BankService bankSpaceService;
	private final Gson gson;

	public CustomerResource(BankService bankService) {
		this.bankSpaceService = bankService;
		gson = new GsonBuilder().create();
	}

	@GET
	public Response getCustomers(@Context UriInfo uriInfo) {
		Response response = null;
		int status = 0;
		String entity = null;
		MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
		try {
			if (queryParams.size() > 0) {
				String customerName = null;
				String customerDocumentId = null;
				for (Entry<String, List<String>> entry : queryParams.entrySet()) {
					if (entry.getKey().equals("customerName"))
						customerName = entry.getValue().get(0);
					else if (entry.getKey().equals("customerDocumentId"))
						customerDocumentId = entry.getValue().get(0);
				}
				if (StringValidator.isValidString(customerName) && StringValidator.isValidString(customerDocumentId)) {
					Customer customer = bankSpaceService.getBankCustomerByIdAndName(customerDocumentId, customerName);
					status = Response.Status.OK.getStatusCode();
					entity = gson.toJson(customer);
				} else {
					status = Response.Status.BAD_REQUEST.getStatusCode();
					entity = "The parameters sent are not allowed.";
				}
			} else {
				List<Customer> customers = bankSpaceService.getAllBankCustomers();
				Collections.sort(customers, new Comparator<Customer>() {
					public int compare(Customer a, Customer b) {
						return a.getCustomerDocumentId().compareToIgnoreCase(b.getCustomerDocumentId())
								+ a.getCustomerName().compareToIgnoreCase(b.getCustomerName());
					}
				});
				status = Response.Status.OK.getStatusCode();
				entity = gson.toJson(customers);
			}

		} catch (NoSuchElementException e) {
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@POST
	@Path("/add")
	public Response addCustomer(Customer customer) {
		Response response = null;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.createBankCustomer(customer);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The customer was created";
		}
		catch(org.openspaces.core.EntryAlreadyInSpaceException e){
			status = Response.Status.CONFLICT.getStatusCode();
			entity = "The customer already exists into the database";
		}
		catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@PUT
	@Path("/update/{customerDocumentId}")
	public Response updateCustomer(@PathParam("customerDocumentId") String customerDocumentId, String customerName) {
		Response response = null;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.updateBankCustomer(customerDocumentId, new Customer(customerName, customerDocumentId, null));
			status = Response.Status.OK.getStatusCode();
			entity = "The customer was updated";
		} catch (NoSuchElementException e) {
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@DELETE
	@Path("/delete/{customerDocumentId}")
	public Response updateCustomer(@PathParam("customerDocumentId") String customerDocumentId) {
		Response response = null;
		int status = 0;
		String entity = null;
		try {
			bankSpaceService.deleteBankCustomer(customerDocumentId);
			status = Response.Status.OK.getStatusCode();
			entity = "The customer was deleted";
		} catch (NoSuchElementException e) {
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		} catch (DataAccessException e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		} catch (Exception e) {
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}
}
