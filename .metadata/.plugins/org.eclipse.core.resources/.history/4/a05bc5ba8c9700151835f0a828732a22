/**
 * 
 */
package com.mera.bank.api.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.openspaces.core.GigaSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gigaspaces.client.WriteModifiers;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.mera.bank.common.entities.Account;
import com.mera.bank.common.entities.Customer;
import com.mera.bank.common.entities.Movement;

/**
 * @author david.mera Implement BankService in order to save data into the space
 */
@Service
@Component("bankSpaceService")
@Qualifier("bankSpaceService")
public class BankSpaceServiceImpl implements BankService {

	@Autowired
	private GigaSpace gigaSpace;

	/**
	 * @see com.mera.bank.api.services.BankService#createBankCustomer(com.mera.bank.common.entities.Customer)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws org.openspaces.core.EntryAlreadyInSpaceException
	 *             if the entry already exists in the space
	 * @throws IllegalArgumentException
	 *             if the passed argument is null
	 */
	@Override
	public void createBankCustomer(Customer newCustomer) {
		if (newCustomer == null) {
			throw new IllegalArgumentException("Null bank customers are not allowed");
		}
		gigaSpace.write(newCustomer, WriteModifiers.WRITE_ONLY);
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getBankCustomerByIdAndName(java.
	 *      lang.String, java.lang.String)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required customer does not exists
	 */
	@Override
	public Customer getBankCustomerByIdAndName(String customerDocumentId, String customerName) {
		Customer customerTemplate = new Customer();
		customerTemplate.setCustomerDocumentId(customerDocumentId);
		customerTemplate.setCustomerName(customerName);
		customerTemplate.setIsProcessed(Boolean.TRUE);
		Customer customer = gigaSpace.read(customerTemplate, 500);
		if (customer == null)
			throw new NoSuchElementException("There is not customers identified by the document " + customerDocumentId
					+ " and named as " + customerName);
		return customer;
	}

	/**
	 * @see com.mera.bank.api.services.BankService#getAllBankCustomers()
	 * @throws org.springframework.dao.DataAccessException
	 *             In the event of a read error, DataAccessException will wrap a
	 *             ReadMultipleException, accessible via
	 *             DataAccessException.getRootCause().
	 */
	@Override
	public List<Customer> getAllBankCustomers() {
		Customer template = new Customer();
		template.setIsProcessed(Boolean.TRUE);
		Customer[] customers = gigaSpace.readMultiple(template, Integer.MAX_VALUE);
		if (customers == null)
			customers = new Customer[0];
		return Arrays.asList(customers);
	}

	/**
	 * @see com.mera.bank.api.services.BankService#getBankCustomerById(java.lang.
	 *      String)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required customer does not exists
	 */
	@Override
	public Customer getBankCustomerById(String customerDocumentId) {
		Customer customer = gigaSpace.readById(Customer.class, customerDocumentId);
		if (customer == null)
			throw new NoSuchElementException("There is not customers identified by the document " + customerDocumentId);
		return customer;
	}

	/***
	 * @see com.mera.bank.api.services.BankService#updateBankCustomer(java.lang.
	 *      String, com.mera.bank.common.entities.Customer)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required customer does not exists
	 */
	@Override
	public void updateBankCustomer(String customerDocumentId, Customer updatedCustomer) {
		Customer customerToUpdate = gigaSpace.readById(Customer.class, customerDocumentId);
		if (!(customerToUpdate == null)) {
			customerToUpdate.setCustomerName(updatedCustomer.getCustomerName());
			gigaSpace.write(customerToUpdate, WriteModifiers.PARTIAL_UPDATE);
		} else {
			throw new NoSuchElementException("There is not customers identified by the document " + customerDocumentId);
		}
	}

	/***
	 * @see com.mera.bank.api.services.BankService#deleteBankCustomer(java.lang.
	 *      String)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required customer does not exists
	 */
	@Override
	public void deleteBankCustomer(String customerDocumentId) {
		Customer customer = gigaSpace.takeById(Customer.class, customerDocumentId);
		if (customer == null)
			throw new NoSuchElementException("There is not customers identified by the document " + customerDocumentId);
	}

	/**
	 * @see com.mera.bank.api.services.BankService#createBankAccount(com.mera.bank.
	 *      common.entities.Account)
	 * @throws org.springframework.dao.DataAccessException
	 * 
	 * @throws org.openspaces.core.EntryAlreadyInSpaceException
	 *             if the entry already exists in the space
	 * @throws IllegalArgumentException
	 *             if a null argument is passed
	 */
	@Override
	public void createBankAccount(Account newAccount) {
		if (newAccount == null) {
			throw new IllegalArgumentException("Null bank accounts are not allowed.");
		}
		gigaSpace.write(newAccount, WriteModifiers.WRITE_ONLY);
	}

	/**
	 * @see com.mera.bank.api.services.BankService#getBankAccountsByCustomerId(java.
	 *      lang.String)
	 * @throws org.springframework.dao.DataAccessException
	 *             In the event of a read error, DataAccessException will wrap a
	 *             ReadMultipleException, accessible via
	 *             DataAccessException.getRootCause().
	 */
	@Override
	public List<Account> getBankAccountsByCustomerId(String customerId) {
		Account template = new Account();
		template.setCustomerDocumentId(customerId);
		List<Account> accounts = Arrays.asList(gigaSpace.readMultiple(template, Integer.MAX_VALUE));
		return accounts;
	}

	/***
	 * @see com.mera.bank.api.services.BankService#getAccountById(java.lang.String)
	 * @throws org.springframework.dao.DataAccessException
	 *             In the event of a read error, DataAccessException will wrap a
	 *             ReadMultipleException, accessible via
	 *             DataAccessException.getRootCause()
	 * @throws NoSuchElementException
	 *             if there is not any account identified by the given criterion
	 * 
	 */
	@Override
	public Account getAccountById(String accountId) {
		Account Account = gigaSpace.readById(Account.class, accountId);
		if (Account == null)
			throw new NoSuchElementException("There is not accounts identified by the id " + accountId);
		return Account;
	}

	/**
	 * @see com.mera.bank.api.services.BankService#getAccountByNumber(java.lang.
	 *      String)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required account does not exists
	 */
	@Override
	public Account getAccountByNumber(String accountNumber) {
		Account template = new Account();
		template.setAccountNumber(accountNumber);
		Account account = gigaSpace.read(template, 500);
		if (account == null) {
			throw new NoSuchElementException("There is not any account identified by the number " + accountNumber);
		}
		return account;
	}

	/**
	 * @see com.mera.bank.api.services.BankService#deleteBankAccount(java.lang.
	 *      String)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws NoSuchElementException
	 *             if the required account does not exists
	 */
	@Override
	public void deleteBankAccount(String accountId) {
		Account Account = gigaSpace.takeById(Account.class, accountId);
		if (Account == null)
			throw new NoSuchElementException("There is not accounts identified by the id " + accountId);
	}

	/**
	 * @see com.mera.bank.api.services.BankService#createBankMovement(com.mera.bank.
	 *      common.entities.Movement)
	 * @throws org.springframework.dao.DataAccessException
	 * @throws org.openspaces.core.EntryAlreadyInSpaceException
	 *             if the entry already exists in the space
	 * @throws IllegalArgumentException
	 *             if a null argument is passed
	 */
	@Override
	public void createBankMovement(Movement newMovement) {
		if (newMovement == null) {
			throw new IllegalArgumentException("Null bank transactions are not allowed.");
		} else {
			gigaSpace.write(newMovement);
		}
	}

	/**
	 * @see com.mera.bank.api.services.BankService#
	 *      getMovementByDateRangeAndAccountNumber(java.lang.String,
	 *      java.util.Date, java.util.Date)
	 * @throws DataAccessException
	 *             In the event of a read error, DataAccessException will wrap a
	 *             ReadMultipleException, accessible via
	 *             DataAccessException.getRootCause().
	 * @throws NullPointerException
	 *             If some date object is null.
	 */
	@Override
	public List<Movement> getMovementByDateRangeAndAccountNumber(String accountNumber, final Date initialDate,
			final Date finalDate) {
		Movement template = new Movement();
		template.setAccountNumber(accountNumber);
		template.setIsProcessed(Boolean.TRUE);
		List<Movement> movements = Arrays.asList(gigaSpace.readMultiple(template, Integer.MAX_VALUE));
		Predicate<Movement> predicateForDate = new Predicate<Movement>() {
			@Override
			public boolean apply(Movement movement) {
				Date testedDate = movement.getMovementDate();
				return (testedDate.after(initialDate) || testedDate.equals(initialDate))
						&& (testedDate.before(finalDate) || testedDate.equals(finalDate));
			}
		};
		java.util.Collection<Movement> movementsInRange = Collections2.filter(movements, predicateForDate);
		return new ArrayList<Movement>(movementsInRange);
	}

}
