<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:os-core="http://www.openspaces.org/schema/core"
	xmlns:os-events="http://www.openspaces.org/schema/events"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.1.xsd
       http://www.openspaces.org/schema/core http://www.openspaces.org/schema/10.2/core/openspaces-core.xsd
       http://www.openspaces.org/schema/events http://www.openspaces.org/schema/10.2/events/openspaces-events.xsd">

	<!-- Spring property configurer which allows us to use system properties 
		(such as user.name). -->
	<bean id="propertiesConfigurer"
		class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer" />

	<!-- A JDBC pooled data source that connects to the HSQL server the mirror 
		starts. -->
	<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"
		destroy-method="close">
		<!-- <property name="driverClassName" value="org.hsqldb.jdbcDriver"/> -->
		<!-- <property name="url" value="jdbc:hsqldb:hsql://localhost/testDB"/> -->
		<!-- <property name="username" value="sa"/> -->
		<!-- <property name="password" value=""/> -->
		<property name="driverClassName" value="org.postgresql.Driver" />
		<property name="url" value="jdbc:postgresql://localhost:5432/banco_db" />
		<property name="username" value="banco_user" />
		<property name="password" value="banco_Password#9" />
	</bean>

	<!-- Hibernate SessionFactory bean. Uses the pooled data source to connect 
		to the database. -->
	<bean id="sessionFactory"
		class="org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean">
		<property name="dataSource" ref="dataSource" />
		<!-- <property name="mappingResources"> -->
		<!-- <list> -->
		<!-- <value>com/mera/bank/common/hibernate.cfg.xml</value> -->
		<!-- </list> -->
		<!-- </property> -->
		<property name="annotatedClasses">
			<list>
				<value>com.mera.bank.common.entities.Customer</value>
				<value>com.mera.bank.common.entities.Account</value>
				<value>com.mera.bank.common.entities.Movement</value>
			</list>
		</property>
		<property name="hibernateProperties">
			<props>
				<!-- SQL dialect -->
				<prop key="hibernate.dialect">org.hibernate.dialect.PostgreSQLDialect</prop>
				<!-- Print executed SQL to stdout -->
				<prop key="show_sql">true</prop>
				<!-- Drop and re-create all database on startup -->
				<prop key="hibernate.hbm2ddl.auto">update</prop>
				<prop key="hibernate.cache.provider_class">org.hibernate.cache.NoCacheProvider</prop>
				<prop key="hibernate.cache.use_second_level_cache">false</prop>
				<prop key="hibernate.cache.use_query_cache">false</prop>
			</props>
		</property>
	</bean>

	<bean id="hibernateSpaceDataSource"
		class="org.openspaces.persistency.hibernate.DefaultHibernateSpaceDataSourceFactoryBean">
		<property name="sessionFactory" ref="sessionFactory" />
		<property name="initialLoadChunkSize" value="2000" />
	</bean>

	<!-- A bean representing a space (an IJSpace implementation). Note, we do 
		not specify here the cluster topology of the space. It is declated outside 
		of the processing unit or within the SLA bean. The space is configured to 
		connect to a mirror, and uses the configured external data source to perform 
		the initial load operation from the database when the Space starts up. localhost:7102/ 
		/./processorSpace -->
	<os-core:space id="space" url="/./bankSpace" schema="persistent"
		mirror="true" space-data-source="hibernateSpaceDataSource"
		lookup-groups="gigaspaces-10.0.1-XAPPremium-ga">
		<os-core:properties>
			<props>
				<!-- Use ALL IN CACHE - Read Only from the database -->
				<prop key="space-config.engine.cache_policy">1</prop>
				<prop key="cluster-config.cache-loader.external-data-source">true</prop>
				<prop key="cluster-config.cache-loader.central-data-source">true</prop>
				<prop key="cluster-config.mirror-service.url">
				jini://*/*/bank-mirror-service
				</prop>
				<!-- /bank-mirror-service_container <prop key="cluster-config.mirror-service.bulk-size">100</prop> -->
				<!-- <prop key="cluster-config.mirror-service.interval-millis">2000</prop> -->
				<!-- <prop key="cluster-config.mirror-service.interval-opers">100</prop> -->
				<!-- <prop key="cluster-config.groups.group.repl-policy.repl-original-state">true</prop> -->
			</props>
		</os-core:properties>
	</os-core:space>

	<!-- Defines a distributed transaction manager. -->
	<os-core:distributed-tx-manager id="transactionManager" />

	<!-- OpenSpaces simplified space API built on top of IJSpace/JavaSpace. -->
	<os-core:giga-space id="gigaSpace" space="space"
		tx-manager="transactionManager" />

	<!-- The data processor bean -->
	<!-- <bean id="dataProcessor" class="com.mera.bank.processor.Processor" 
		/> -->

	<!-- A polling event container that perfoms (by default) polling take operations 
		against the space using the provided template (in our case, and the non processed 
		data objects). Once a match is found, the data processor bean event listener 
		is triggered using the method adapter. -->
	<!-- <os-events:polling-container id="dataProcessorPollingEventContainer" 
		giga-space="gigaSpace"> -->
	<!-- <os-events:tx-support tx-manager="transactionManager"/> -->
	<!-- <os-core:template> -->
	<!-- <bean class="com.mera.bank.common.Data"> -->
	<!-- <property name="processed" value="false"/> -->
	<!-- </bean> -->
	<!-- </os-core:template> -->
	<!-- <os-events:listener> -->
	<!-- <os-events:method-adapter method-name="processData"> -->
	<!-- <os-events:delegate ref="dataProcessor"/> -->
	<!-- </os-events:method-adapter> -->
	<!-- </os-events:listener> -->
	<!-- </os-events:polling-container> -->

	<bean id="customerProcessor" class="com.mera.bank.processor.CustomerProcessor" />
	<bean id="accountProcessor" class="com.mera.bank.processor.AccountProcessor"></bean>

	<os-events:polling-container id="customerProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">
		<os-events:tx-support tx-manager="transactionManager" />
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Customer">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="saveCustomer">
				<os-events:delegate ref="customerProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

	<os-events:polling-container id="accountProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">
		<os-events:tx-support tx-manager="transactionManager" />
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Account">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="saveAccount">
				<os-events:delegate ref="accountProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

	<bean id="transactionProcessor" class="com.mera.bank.processor.TransactionProcessor" />

	<os-events:polling-container id="TransactionProcessorPollingContainer"
		giga-space="gigaSpace" concurrent-consumers="3"
		max-concurrent-consumers="5">
		<os-events:tx-support tx-manager="transactionManager" />
		<os-core:template>
			<bean class="com.mera.bank.common.entities.Movement">
				<property name="isProcessed" value="false"></property>
			</bean>
		</os-core:template>
		<os-events:listener>
			<os-events:method-adapter method-name="processTransaction">
				<os-events:delegate ref="transactionProcessor"></os-events:delegate>
			</os-events:method-adapter>
		</os-events:listener>
	</os-events:polling-container>

</beans>