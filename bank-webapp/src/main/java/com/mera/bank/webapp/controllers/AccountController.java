/**
 * 
 */
package com.mera.bank.webapp.controllers;

import java.math.BigDecimal;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.mera.bank.webapp.services.AccountService;
import com.mera.bank.webapp.services.CustomerService;
import com.mera.bank.webapp.models.AccountModel;
import com.mera.bank.webapp.models.CustomerModel;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Controller which manage request for bank accounts
 * @author david.mera
 *
 */
public class AccountController extends SelectorComposer<Component> {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -8481262019591827751L;
	/***
	 * An instance of Logger
	 */
	private static final Logger LOGGER = Logger.getLogger(AccountController.class.getName());
    /***
     * A textbox to capture an account number when the user wants to create a new bank account
     */
	@Wire
	private Textbox accountNumber;
	/***
	 * A Listbox which displays the available customers to associate the new bank account to be created
	 */
	@Wire
	private Listbox customerList;
	/***
	 * A listbox which displays the bank accounts filtered by a given criteria
	 */
	@Wire
	private Listbox accountList;

	/***
	 * A textbox which allows to filter the accounts by a given number
	 */
	@Wire
	private Textbox accountNumberFilter;
	/***
	 * A Listbox which displays the available customers to filter the accounts
	 */
	@Wire
	private Listbox customerListFilter;
	/***
	 * A button to confirm the request for creating a new bank account
	 */
	@Wire
	private Button addAccountBtn;
	/***
	 * A Button to filter the accounts by a given criteria
	 */
	@Wire
	private Button filterAccountBtn;

	/*
	 * (non-javadoc)
	 * @see org.zkoss.zk.ui.select.SelectorComposer.doAfterCompose(T comp)
	 */
	@Override
	public void doAfterCompose(Component component) throws Exception {
		super.doAfterCompose(component); // wire variables and event listeners
		if ("parentLayout".equals(component.getId())) {
			ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(
					CustomerService.getBankCustomers());
			customerModel.addToSelection(customerModel.get(0));
			customerList.setModel(customerModel);
			customerListFilter.setModel(customerModel);
			if (!customerModel.isEmpty()) {
				addAccountBtn.setDisabled(false);
				customerList.setSelectedItem(customerList.getItemAtIndex(0));
				customerListFilter.setSelectedItem(customerListFilter.getItemAtIndex(0));
			}
			filterAccounts();
		}
	}
	
	/***
	 * This method is triggered when the user confirms to create a new bank account
	 */
	@Listen("onClick = #addAccountBtn")
	public void addAccount(){
		String customerDocumentId = customerList.getSelectedItem().getValue();
		AccountModel account = new AccountModel(null, accountNumber.getValue(), BigDecimal.ZERO, customerDocumentId, false);
		ClientResponse response = AccountService.addBankAccount(account);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_CREATED) {
				Messagebox.show(entity, "Add Account", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to add account. HTTP %d - %s", status, entity));
				Messagebox.show(entity, "Failed Add Account", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			Messagebox.show("It is not posible connect to server", "Failed Add Account", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
	
	/***
	 * This method is triggered when the user wants to filter bank accounts by a given account number or a specific customer
	 */
	@Listen("onChanging = #accountNumberFilter; onSelect = #customerListFilter; onClick = #filterAccountBtn")
	public void filterAccounts(){
		String customerDocumentId = customerListFilter.getSelectedItem().getValue();
		String number = accountNumberFilter.getValue();
		AccountModel[] accounts =  AccountService.getAccountsByNumberAndCustomerId(number, customerDocumentId);
		ListModelList<AccountModel> accountModel = new ListModelList<AccountModel>(accounts);
		accountList.setModel(accountModel);
	}
	
	/***
	 * This method is triggered when the user confirms the request for deleting a bank account
	 * @param event	An instance of onClick event
	 */
	@Listen("onClick = #deleteAccountBtn")
	public void deleteAccount(final Event event){
		Messagebox.show("Are you sure you want to delete?", "Alert !!", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener<Event>() {
					@Override
					public void onEvent(Event evt) throws InterruptedException {
						if ("onYes".equals(evt.getName())) {
							confirmedDeleteAccount(event);
						}
					}
				});
	}
	
	/***
	 * This method is triggered when the user wants to associate a new movement to a specific bank account
	 * @param event An instance of onClick event
	 */
	@Listen("onClick = #addMovementBtn")
	public void addMovement(Event event){
		HashMap<String, Object> map = new HashMap<String, Object>();
		AccountModel selectedAccount = (AccountModel) event.getData();
		map.put("accountNumber", selectedAccount.getAccountNumber());
		Window window = (Window) Executions.createComponents("newMovementDialog.zul", null, map);
		window.doModal();
	}
	
	/***
	 * This method process the response for a request of delete an account.
	 * It displays the result returned by bank API Rest
	 * @param event An Event instance
	 */
	private void confirmedDeleteAccount(Event event) {
		String accountId = (String) event.getData();
		ClientResponse response = AccountService.deleteBankAccount(accountId);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				Messagebox.show(entity, "Delete Account", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to delete account %s. HTTP %d - %s", accountId,
						status, entity));
				Messagebox.show(entity, "Failed Delete Account", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			Messagebox.show("It is not posible to connect to server", "Failed Delete Account", Messagebox.OK,
					Messagebox.ERROR);
		}
	}

}
