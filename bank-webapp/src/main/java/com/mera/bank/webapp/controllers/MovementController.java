/**
 * 
 */
package com.mera.bank.webapp.controllers;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.joda.time.DateTime;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.mera.bank.webapp.services.AccountService;
import com.mera.bank.webapp.services.CustomerService;
import com.mera.bank.webapp.services.MovementService;
import com.mera.bank.webapp.models.AccountModel;
import com.mera.bank.webapp.models.CustomerModel;
import com.mera.bank.webapp.models.MovementModel;
import com.mera.bank.webapp.models.MovementType;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Controller which manage request for bank customers
 * @author david.mera
 *
 */
public class MovementController extends SelectorComposer<Component> {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -5017522068637032386L;
	/***
	 * Instance of logger
	 */
	private static final Logger LOGGER = Logger.getLogger(MovementController.class.getName());

	/***
	 * The new movement dialog
	 */
	@Wire
	private Window newMovementDialog;
	/**
	 * A label for account number in the new movement dialog
	 */
	@Wire
	private Label accountNumber;
	/***
	 * A combobox for movement types in the new movement dialog
	 */
	@Wire
	private Combobox movementType;
	/***
	 * A doublebox for movement value in the new movement dialog
	 */
	@Wire
	private Doublebox movementValue;
	/***
	 * A button which confirms the new movement
	 */
	@Wire
	private Button addMovementBtn;
	/***
	 * A button which cancels the request for the new movement
	 */
	@Wire
	private Button cancelMovementBtn;
	/***
	 * A button which filters movements by a given criteria
	 */
	@Wire
	private Button filterMovementsBtn;
	/**
	 * A Textbox to filter movements by an account number
	 */
	@Wire
	private Textbox accountNumberFilter;
	/**
	 * A Datebox to filter movements after or since the given date
	 */
	@Wire
	private Datebox initialDateFilter;
	/**
	 * A Datebox to filter movements before or until the given date
	 */
	@Wire
	private Datebox finalDateFilter;
	/**
	 * A label which contains the account number of the filtered movements
	 */
	@Wire
	private Label accountNumberFiltered;
	/**
	 * A label which contains the customer ID who owns the account which has the filtered movements
	 */
	@Wire
	private Label accountCustomerIdFiltered;
	/**
	 * A label which contains the account balance for the filtered movements
	 */
	@Wire
	private Label accountBalanceFiltered;
	/**
	 * The listbox which displays the filtered movements
	 */
	@Wire
	private Listbox movementList;
	
	/***
	 * The parent border layout on the report of movements view
	 */
	@Wire
	private Borderlayout reportParenLayout;
	/***
	 * A listbox to filter accounts by customer
	 */
	@Wire
	private Listbox customersReportFilter;
	/***
	 * A datebox to filter the movements after or since the given date
	 */
	@Wire
	private Datebox initialReportDateFilter;
	/***
	 * A datebox to filter the movements before or until the given date
	 */
	@Wire
	private Datebox finalReportDateFilter;
	/***
	 * The button which starts the request of movements by the given criteria
	 */
	@Wire
	private Button reportFilterBtn;
	/**
	 * A listbox to display the customer's accounts
	 */
	@Wire
	private Listbox accountsReportList;
	/**
	 * A listbox to display the movements of each customer's account
	 */
	@Wire
	private Listbox movementsReportList;
	
	/*
	 * (non-javadoc)
	 * @see org.zkoss.zk.ui.select.SelectorComposer.doAfterCompose(T comp)
	 */
	@Override
	public void doAfterCompose(Component component) throws Exception {
		super.doAfterCompose(component); // wire variables and event listeners
		if ("reportParenLayout".equals(component.getId())) {
			ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(
					CustomerService.getBankCustomers());
			customerModel.addToSelection(customerModel.get(0));
			customersReportFilter.setModel(customerModel);
			if (!customerModel.isEmpty()) {
				reportFilterBtn.setDisabled(false);
				customersReportFilter.setSelectedItem(customersReportFilter.getItemAtIndex(0));
			}
		}
	}
	
	/***
	 * This method is triggered when the user wants to create a new movement
	 * @param event An instance of CreateEvent event
	 */
	@Listen("onCreate = #newMovementDialog")
	public void showDialog(CreateEvent event) {
		accountNumber.setValue((String) event.getArg().get("accountNumber"));
		movementType.setSelectedIndex(0);
	}
	
	/***
	 * This method is triggered when the user confirms to add a movement
	 */
	@Listen("onClick = #addMovementBtn")
	public void addMovement(){
		String number = accountNumber.getValue();
		String typeMovement = movementType.getValue();
		BigDecimal value = new BigDecimal(movementValue.getValue());
		newMovementDialog.detach();
		MovementModel newMovement = new MovementModel();
		newMovement.setAccountNumber(number);
		newMovement.setMovementType(MovementType.valueOf(typeMovement));
		newMovement.setMovementValue(value);
		ClientResponse response = MovementService.addMovement(newMovement);
		if(response != null){
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_CREATED) {
				Messagebox.show("The bank movement will be processed. You can check the account balance in movements report.", "Add Movement", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to add movement. HTTP %d - %s", status, entity));
				Messagebox.show(entity, "Failed Add Customer", Messagebox.OK, Messagebox.ERROR);
			}
		}
		else{
			Messagebox.show("It is not posible connect to server", "Failed Add Customer", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
	
	/***
	 * This method is triggered when the user regrets to add a movement 
	 */
	@Listen("onClick = #cancelMovementBtn")
	public void cancelMovement(){
		newMovementDialog.detach();
	}
	
	/***
	 * This method is triggered when the user wants to get movements by a given criteria
	 */
	@Listen("onClick = #filterMovementsBtn")
	public void getMovements(){
		String number = accountNumberFilter.getValue();
		Date finalDate = new DateTime(finalDateFilter.getValue()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
		Date initialDate = new DateTime(initialDateFilter.getValue()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate();
		ListModelList<MovementModel> movementModel = new ListModelList<MovementModel>(MovementService.getMovementsByDateAndAccountNumber(initialDate, finalDate, number));
		// Account info
		AccountModel account = AccountService.getAccountByNumber(number);
		if(!movementModel.isEmpty()){				
			if(account == null){
			movementList.setModel(movementModel);
			Messagebox.show("An error occurs trying to get information about the account from server", "Failed Get Account Information", Messagebox.OK, Messagebox.ERROR);
			}
			else{
				accountNumberFiltered.setValue(account.getAccountNumber());
				accountCustomerIdFiltered.setValue(account.getCustomerDocumentId());
				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(6);
				df.setMinimumFractionDigits(0);
				df.setGroupingUsed(true);			
				accountBalanceFiltered.setValue(String.format("$ %s", df.format(account.getAccountBalance())));
				movementList.setModel(movementModel);
			}
		}
		else{
			if(account == null){
				Messagebox.show("The required account are not registered on the database", "Failed Get Movements", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}

	@Listen("onClick = #reportFilterBtn")
	public void generateReport(){
		String customerId = customersReportFilter.getSelectedItem().getValue();
		Date finalDate = new DateTime(finalReportDateFilter.getValue()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).toDate();
		Date initialDate = new DateTime(initialReportDateFilter.getValue()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).toDate();
		//** Get accounts
		AccountModel[] accounts = AccountService.getAccountsByNumberAndCustomerId(null, customerId);
		//** Get all movements by customer
		List<MovementModel> movements = new ArrayList<MovementModel>();
		for(AccountModel account :  accounts){
			movements.addAll(Arrays.asList(MovementService.getMovementsByDateAndAccountNumber(initialDate, finalDate, account.getAccountNumber())));
		}
		//** Display data
		ListModelList<AccountModel> accountModel = new ListModelList<AccountModel>(accounts);
		ListModelList<MovementModel> movementModel = new ListModelList<MovementModel>(movements);
		accountsReportList.setModel(accountModel);
		movementsReportList.setModel(movementModel);
	}
}