/**
 * 
 */
package com.mera.bank.webapp.controllers;

import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.mera.bank.webapp.services.CustomerService;
import com.mera.bank.webapp.models.CustomerModel;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Controller which manage request for bank customers
 * @author david.mera
 *
 */
public class CustomerController extends SelectorComposer<Component> {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -2366073496443917093L;
	/***
	 * Instance of logger
	 */
	private static final Logger LOGGER = Logger.getLogger(CustomerController.class.getName());

	/**
	 * A Textbox provided to save the name of the new customer to be created
	 */
	@Wire
	private Textbox customerName;
	/**
	 * A Textbox provided to save the ID of the new customer to be created
	 */
	@Wire
	private Textbox customerId;
	/**
	 * This Listbox displays the created customers
	 */
	@Wire
	private Listbox customerList;
	/***
	 * A label which displays the name of a created customer
	 */
	@Wire
	private Label customerNameLabel;
	/***
	 * A label which displays the ID of a created customer
	 */
	@Wire
	private Label customerDocumentIdLabel;
	/***
	 * Parent layout object in customer's view
	 */
	@Wire
	private Borderlayout parentLayout;
	/**
	 * A button which confirms the request to update customer's information
	 */
	@Wire
	private Button updateCustomerBtn;
	/**
	 * A window dialog which captures the request to update customer's information
	 */
	@Wire
	private Window updateCustomerDialog;
	/**
	 * A label which displays the customer ID in update customer dialog
	 */
	@Wire
	private Label updatedCustomerId;
	/**
	 * A Textbox to set the customer name in update customer dialog
	 */
	@Wire
	private Textbox updatedCustomerName;

	/*
	 * (non-javadoc)
	 * @see org.zkoss.zk.ui.select.SelectorComposer.doAfterCompose(T comp)
	 */
	@Override
	public void doAfterCompose(Component component) throws Exception {
		super.doAfterCompose(component); // wire variables and event listeners
		if("parentLayout".equals(component.getId())){
		ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(
				CustomerService.getBankCustomers());
		customerList.setModel(customerModel);
		}
	}

	/***
	 * This method is triggered when the timer is activated a certain interval of time 
	 */
	@Listen("onTimer = #timer")
	public void onTimer() {
		customerList.setModel(new ListModelList<CustomerModel>(CustomerService.getBankCustomers()));
	}

	/***
	 * This method is triggered when the user confirms the request for creating a new customer
	 */
	@Listen("onClick = #addCustomerBtn")
	public void addCustomer() {
		CustomerModel customer = new CustomerModel(customerId.getValue(), customerName.getValue(), false);
		ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(new CustomerModel[0]);
		ClientResponse response = CustomerService.addBankCustomer(customer);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_CREATED) {
				customerModel = new ListModelList<CustomerModel>(CustomerService.getBankCustomers());
				Messagebox.show(entity, "Add Customer", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to add customer. HTTP %d - %s", status, entity));
				Messagebox.show(entity, "Failed Add Customer", Messagebox.OK, Messagebox.ERROR);
			}
			customerList.setModel(customerModel);
		} else {
			Messagebox.show("It is not posible connect to server", "Failed Add Customer", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
	
	/***
	 * This method is triggered when the user starts the request for updating the customer's information
	 * @param event An instance of onCreate event
	 */
	@Listen("onClick = #modifyCustomerBtn")
	public void updateCustomer(Event event) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		CustomerModel selectedCustomer = (CustomerModel) event.getData();
		map.put("documentId", selectedCustomer.getCustomerDocumentId());
		map.put("name", selectedCustomer.getCustomerName());
		Window window = (Window) Executions.createComponents("updateCustomerDialog.zul", null, map);
		window.doModal();
	}

	/***
	 * This method is triggered when the user confirms the request for deleting the customer's information
	 * @param event An instance of onClick event
	 */
	@Listen("onClick = #deleteCustomerBtn")
	public void deleteCustomer(final Event event) {
		Messagebox.show("Are you sure you want to delete?", "Alert !!", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener<Event>() {
					@Override
					public void onEvent(Event evt) throws InterruptedException {
						if ("onYes".equals(evt.getName())) {
							confirmedDeleteCustomer(event);
						}
						ListModelList<CustomerModel> customerModel = new ListModelList<CustomerModel>(
								CustomerService.getBankCustomers());
						customerList.setModel(customerModel);
					}
				});
	}

	/**
	 * This event is triggered when the update customer dialog is displayed
	 * @param event An instance of onCreate event
	 */
	@Listen("onCreate = #updateCustomerDialog")
	public void showDialog(CreateEvent event) {
		updatedCustomerId.setValue((String) event.getArg().get("documentId"));
		updatedCustomerName.setValue((String) event.getArg().get("name"));
	}

	/***
	 * This method is triggered when the user confirms the request for updating the customer's information
	 * @param event An instance of onClick event
	 */
	@Listen("onClick = #updateCustomerBtn")
	public void updateCustomer() {
		String newName = updatedCustomerName.getValue();
		String customerDocumentId = updatedCustomerId.getValue();
		ClientResponse response = CustomerService.updateBankCustomer(customerDocumentId, newName);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				Messagebox.show(entity, "Update Customer", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to update customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
				Messagebox.show(entity, "Failed Update Customer", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			Messagebox.show("It is not posible connect to server", "Failed Update Customer", Messagebox.OK,
					Messagebox.ERROR);
		}
		updateCustomerDialog.detach();
	}

	/***
	 * This method is triggered when the user regrets the request for updating the customer's information
	 * @param event An instance of onClick event
	 */
	@Listen("onClick = #cancelUpdateBtn")
	public void cancelUpdate() {
		updateCustomerDialog.detach();
	}

	/***
	 * This method process the response for a request of delete a customer.
	 * It displays the result returned by bank API Rest
	 * @param event An Event instance
	 */
	private void confirmedDeleteCustomer(Event event) {
		String customerDocumentId = (String) event.getData();
		ClientResponse response = CustomerService.deleteBankCustomer(customerDocumentId);
		if (response != null) {
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				Messagebox.show(entity, "Delete Customer", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				LOGGER.error(String.format("Failed attempt to delete customer %s. HTTP %d - %s", customerDocumentId,
						status, entity));
				Messagebox.show(entity, "Failed Delete Customer", Messagebox.OK, Messagebox.ERROR);
			}
		} else {
			Messagebox.show("It is not posible to connect to server", "Failed Delete Customer", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
}