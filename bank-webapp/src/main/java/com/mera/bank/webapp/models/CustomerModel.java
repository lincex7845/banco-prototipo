/**
 * 
 */
package com.mera.bank.webapp.models;

/**
 * POJO which represents a bank customer
 * @author david.mera
 *
 */
public class CustomerModel {

	/****
	 * The customer identifier
	 */
	private String customerDocumentId;
	/***
	 * The customer name
	 */
	private String customerName;
	/**
	 * This flag sets if the customer was processed or not
	 */
	private Boolean isProcessed;
	
	/***
	 * constructor by default
	 */
	public CustomerModel(){
		//constructor by default
	}
	
	/***
	 * Parameterized constructor
	 * @param customerDocumentId	The document ID
	 * @param customerName			The name of the customer
	 * @param isProcessed			It sets if the customer has been processed
	 */
	public CustomerModel(String customerDocumentId, String customerName, Boolean isProcessed){
		this.setCustomerDocumentId(customerDocumentId);
		this.setCustomerName(customerName);
		this.isProcessed = isProcessed;
	}

	/**
	 * @return the customerDocumentId
	 */
	public String getCustomerDocumentId() {
		return customerDocumentId;
	}
	
	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}
	
	/**
	 * @return the isProcessed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @param customerDocumentId the customerDocumentId to set
	 */
	public void setCustomerDocumentId(String customerDocumentId) {
		this.customerDocumentId = customerDocumentId;
	}
	
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	/**
	 * @param isProcessed the isProcessed to set
	 */
	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	
}
