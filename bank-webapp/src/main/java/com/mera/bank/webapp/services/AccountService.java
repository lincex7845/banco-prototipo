/**
 * 
 */
package com.mera.bank.webapp.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import static java.util.Arrays.asList;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.commons.lang3.CharEncoding;
import org.jboss.logging.Logger;
import com.google.gson.Gson;
import com.mera.bank.webapp.utilities.StringValidator;
import com.mera.bank.webapp.models.AccountModel;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * This class wraps a set of functions which make HTTP requests to bank API
 * in order to manipulate bank accounts
 * @author david.mera
 *
 */
public class AccountService {

	/**
	 * Instance of Gson which returns JSON objects
	 */
	private static Gson gson;
	/***
	 * An HTTP Client provided by Jersey
	 */
	private static Client client;
	/***
	 * Domain of the bank API Rest
	 */
	private static final String SERVICEHOST;
	/***
	 * Instance of logger for this class
	 */
	private static final Logger LOGGER;
	/**
	 * Timeout for HTTP requests
	 */
	private static final Integer TIMEOUT;

	static {
		gson = new Gson();
		client = Client.create();
		TIMEOUT = new Integer(3000);
		client.setReadTimeout(TIMEOUT);
		SERVICEHOST = "http://localhost:9000";
		LOGGER = Logger.getLogger(AccountService.class.getName());
	}

	private AccountService() {

	}

	/***
	 * This method performs a HTTP POST request in order to create a bank account
	 * @param account	The new bank account to create
	 * @return {@link com.sun.jersey.api.client.ClientResponse}	This object wraps the HTTP response, its status code and its body 
	 */
	public static ClientResponse addBankAccount(AccountModel account) {
		ClientResponse response = null;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/accounts/add");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				gson.toJson(account));
		LOGGER.info("Request for adding the account: "
				+ String.format("%s - %s", account.getCustomerDocumentId(), account.getAccountNumber()));
		try {
			response = res.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to add account: ", e);
		}
		return response;
	}

	/***
	 * This method performs a HTTP GET request in order to obtain a bank account
	 * @param accountNumber		The account number which identifies the account
	 * @param customerDocumentId	The customer which owns the account
	 * @return {@link com.mera.bank.webapp.models.AccountModel[]}	An array which contains the required account 
	 */
	public static AccountModel[] getAccountsByNumberAndCustomerId(String accountNumber, String customerDocumentId) {
		AccountModel[] accounts = new AccountModel[0];
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/accounts");
		try {
			MultivaluedMap<String, String> parameters = new MultivaluedMapImpl();
			if (StringValidator.isValidString(accountNumber))
				parameters.put("accountNumber", asList(URLEncoder.encode(accountNumber, CharEncoding.UTF_8)));
			if (StringValidator.isValidString(customerDocumentId))
				parameters.put("customerDocumentId", asList(URLEncoder.encode(customerDocumentId, CharEncoding.UTF_8)));
			Future<ClientResponse> res = awr.queryParams(parameters).type(MediaType.APPLICATION_JSON_TYPE)
					.get(ClientResponse.class);
			LOGGER.info(
					"Request for getting accounts by customer " + customerDocumentId + " and number " + accountNumber);
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				accounts = gson.fromJson(entity, AccountModel[].class);
			} else {
				LOGGER.error(String.format("Failed attempt to get accounts. HTTP %d - %s", status, entity));
			}
		} catch (InterruptedException | ExecutionException | UnsupportedEncodingException e) {
			LOGGER.error("Raised exception when trying to get accounts: ", e);
		}
		return accounts;
	}

	/***
	 * This method makes a HTTP DELETE request to remove a bank account
	 * @param accountId	The account identifier
	 * @return {@link com.sun.jersey.api.client.ClientResponse}	This object wraps the HTTP response, its status code and its body 
	 */
	public static ClientResponse deleteBankAccount(String accountId) {
		ClientResponse response = null;
		try {
			AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/accounts/delete");
			MultivaluedMap<String, String> parameters = new MultivaluedMapImpl();
			if (StringValidator.isValidString(accountId))
				parameters.put("accountId", asList(URLEncoder.encode(accountId, CharEncoding.UTF_8)));
			Future<ClientResponse> res = awr.queryParams(parameters).type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
			LOGGER.info("Request for deleting account " + accountId);
			response = res.get();
		} catch (InterruptedException | ExecutionException | UnsupportedEncodingException e) {
			LOGGER.error("Raised exception when trying to delete a customer: ", e);
		}
		return response;
	}

	/***
	 * This method performs a HTTP GET request in order to obtain a bank account
	 * @param accountNumber		The account number which identifies the account
	 * @return {@link com.mera.bank.webapp.models.AccountModel}	The account which satisfies the given criterion 
	 */
	public static AccountModel getAccountByNumber(String accountNumber) {
		AccountModel account = null;
		try {
			AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/accounts/byAccountNumber/" + accountNumber);
			Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
			LOGGER.info("Request for getting account by number " + accountNumber);
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				account = gson.fromJson(entity, AccountModel.class);
			} else {
				LOGGER.error(String.format("Failed attempt to get the account. HTTP %d - %s", status, entity));
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to get accounts: ", e);
		}
		return account;
	}
}
