/**
 * 
 */
package com.mera.bank.webapp.models;

import java.math.BigDecimal;

/**
 * POJO which represents a bank account
 * @author david.mera
 *
 */
public class AccountModel {

	/***
	 * The account identifier
	 */
	private String id;
	/**
	 * The account number which identifies it as well
	 */
	private String accountNumber;
	/***
	 * The account balance
	 */
	private BigDecimal accountBalance;
	/**
	 * The customer Id which the account belongs to
	 */
	private String customerDocumentId;
	/***
	 * This flag sets if the account was processed or not
	 */
	private Boolean isProcessed;
	
	/***
	 * Constructor by default
	 */
	public AccountModel(){
		//constructor by default
	}
	
	/***
	 * Parameterized constructor
	 * @param id				The account identifier
	 * @param accountNumber		The account number
	 * @param accountBalance	The account balance
	 * @param customerDocumentId	It identifies which customer owns the account
	 * @param isProcessed		It sets if the account has been processed or not
	 */
	public AccountModel(String id, String accountNumber, BigDecimal accountBalance, 
			String customerDocumentId, Boolean isProcessed){
		this.id = id;
		this.accountNumber = accountNumber;
		this.accountBalance = accountBalance;
		this.customerDocumentId = customerDocumentId;
		this.isProcessed = isProcessed;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountBalance
	 */
	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	/**
	 * @param accountBalance the accountBalance to set
	 */
	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	/**
	 * @return the customerDocumentId
	 */
	public String getCustomerDocumentId() {
		return customerDocumentId;
	}

	/**
	 * @param customerDocumentId the customerDocumentId to set
	 */
	public void setCustomerDocumentId(String customerDocumentId) {
		this.customerDocumentId = customerDocumentId;
	}

	/**
	 * @return the isProcessed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @param isProcessed the isProcessed to set
	 */
	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	
}
