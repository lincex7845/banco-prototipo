/**
 * 
 */
package com.mera.bank.webapp.services;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.mera.bank.webapp.models.CustomerModel;
import com.sun.jersey.api.client.AsyncWebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import org.jboss.logging.Logger;

/**
 * This class wraps a set of functions which make HTTP requests to bank API
 * in order to manipulate bank customers
 * @author david.mera
 *
 */
public class CustomerService {

	/**
	 * Instance of Gson which returns JSON objects
	 */
	private static Gson gson;
	/***
	 * An HTTP Client provided by Jersey
	 */
	private static Client client;
	/***
	 * Domain of the bank API Rest
	 */
	private static final String SERVICEHOST;
	/***
	 * Instance of logger for this class
	 */
	private static final Logger LOGGER;
	/**
	 * Timeout for HTTP requests
	 */
	private static final Integer TIMEOUT;

	static {
		gson = new Gson();
		client = Client.create();
		TIMEOUT = new Integer(3000);
		client.setReadTimeout(TIMEOUT);
		SERVICEHOST = "http://localhost:9000";
		LOGGER = Logger.getLogger(CustomerService.class.getName());
	}

	private CustomerService() {

	}

	/***
	 * This method performs a HTTP POST request in order to create a bank customer
	 * @param customerModel	The new bank customer to be created
	 * @return {@link com.sun.jersey.api.client.ClientResponse}	This object wraps the HTTP response, its status code and its body 
	 */
	public static ClientResponse addBankCustomer(CustomerModel customerModel) {
		ClientResponse response = null;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/add");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class,
				gson.toJson(customerModel));
		LOGGER.info("Request for adding the customer: "
				+ String.format("%s - %s", customerModel.getCustomerDocumentId(), customerModel.getCustomerName()));
		try {
			response = res.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to add customer: ", e);
		}
		return response;
	}

	/***
	 * This method performs a HTTP GET request in order to obtain all bank customers
	 * @return {@link com.mera.bank.webapp.models.CustomerModel[]}	An array which contains the required customers 
	 */
	public static CustomerModel[] getBankCustomers() {
		CustomerModel[] customers = new CustomerModel[0];
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers");
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		LOGGER.info("Request for getting customers");
		try {
			ClientResponse response = res.get();
			int status = response.getStatus();
			String entity = response.getEntity(String.class);
			if (status == HttpServletResponse.SC_OK) {
				customers = gson.fromJson(entity, CustomerModel[].class);
			} else {
				LOGGER.error(String.format("Failed attempt to get customers. HTTP %d - %s", status, entity));
			}
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to get customers: ", e);
		}
		return customers;
	}

	/***
	 * This method performs a HTTP POST request in order to update a bank customer
	 * @param customerDocumentId The customer identifier
	 * @param customerModel	The new bank customer to be updated
	 * @return {@link com.sun.jersey.api.client.ClientResponse}	This object wraps the HTTP response, its status code and its body 
	 */
	public static ClientResponse updateBankCustomer(String customerDocumentId, String customerNewName) {
		ClientResponse response = null;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/update/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class,
				customerNewName);
		LOGGER.info("Request for updating customer " + customerDocumentId);
		try {
			response = res.get();
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to update a customer: ", e);
		}
		return response;
	}

	/***
	 * This method performs a HTTP POST request in order to remove a bank customer
	 * @param customerDocumentId The customer identifier
	 * @return {@link com.sun.jersey.api.client.ClientResponse}	This object wraps the HTTP response, its status code and its body 
	 */
	public static ClientResponse deleteBankCustomer(String customerDocumentId) {
		ClientResponse response = null;
		AsyncWebResource awr = client.asyncResource(SERVICEHOST + "/customers/delete/" + customerDocumentId);
		Future<ClientResponse> res = awr.type(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
		LOGGER.info("Request for deleting customer " + customerDocumentId);
		try {
			response = res.get();			
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.error("Raised exception when trying to delete a customer: ", e);
		}
		return response;
	}
}
