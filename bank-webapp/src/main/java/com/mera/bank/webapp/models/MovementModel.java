/**
 * 
 */
package com.mera.bank.webapp.models;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO which represent a bank movement
 * @author david.mera
 *
 */
public class MovementModel {

	/***
	 * The movement identifier
	 */
	private String id;
	/***
	 * The account number which the movement belongs to
	 */
	private String accountNumber;
	/***
	 * The date when the movement happens
	 */
	private Date movementDate;
	/***
	 * The amount of money 
	 */
	private BigDecimal movementValue;
	/***
	 * The type of movement, it can be a CREDIT or DEBIT
	 */
	private MovementType movementType;
	/***
	 * This flag sets if the movement was processed or not
	 */
	private Boolean isProcessed;
	
	/***
	 * constructor by default
	 */
	public MovementModel(){
		//constructor by default
	}
	
	/***
	 * Parameterized constructor
	 * @param id				The movement's identifier
	 * @param accountNumber		The account number
	 * @param movementDate		The date when the movement happen
	 * @param movementValue		The amount of money
	 * @param movementType		Type of movement, debit or credit
	 * @param isProcessed		Flag which sets if the movement has been processed or not
	 */
	public MovementModel(String id, String accountNumber, Date movementDate, 
			BigDecimal movementValue, MovementType movementType, Boolean isProcessed){
		this.id = id;
		this.accountNumber = accountNumber;
		this.movementDate = movementDate;
		this.movementValue = movementValue;
		this.movementType = movementType;
		this.isProcessed = isProcessed;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
		
	/**
	 * @return the movementDate
	 */
	public Date getMovementDate() {
		return movementDate;
	}
	
	/**
	 * @return the movementValue
	 */
	public BigDecimal getMovementValue() {
		return movementValue;
	}
	
	/**
	 * @return the movementType
	 */
	public MovementType getMovementType() {
		return movementType;
	}
	
	/**
	 * @return the isProcessed
	 */
	public Boolean getIsProcessed() {
		return isProcessed;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @param movementDate the movementDate to set
	 */
	public void setMovementDate(Date movementDate) {
		this.movementDate = movementDate;
	}
	/**
	 * @param movementValue the movementValue to set
	 */
	public void setMovementValue(BigDecimal movementValue) {
		this.movementValue = movementValue;
	}
	/**
	 * @param movementType the movementType to set
	 */
	public void setMovementType(MovementType movementType) {
		this.movementType = movementType;
	}
	
	/**
	 * @param isProcessed the isProcessed to set
	 */
	public void setIsProcessed(Boolean isProcessed) {
		this.isProcessed = isProcessed;
	}
	
	
}
